﻿using System;
using System.Drawing;
using CFAN;
using CFAN.UI.ViewModel;

namespace Test
{
	class Program
	{
		static void Main(string[] args)
		{
			new Program();
		}

		private Program ()
		{
			var context = new AppContext();
			
			
			var id = context.Profile.GetProfileIdFromArchive("Test.zip");

			var existing = context.Profile.GetById(id);

			var created = context.Profile.CreateFromExport("Test.zip", "Test Imported ProfileView").Result;
		
			Console.WriteLine();
		}
	}
}
