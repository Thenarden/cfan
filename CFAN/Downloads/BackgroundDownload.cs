﻿using System;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using CFAN.Exceptions;
using CFAN.Utils;
using PropertyChanged;
using Shortbit.Utils;

namespace CFAN.Downloads
{
	public class BackgroundDownload : INotifyPropertyChanged
	{
		public interface IProgressInfo
		{
			long Downloaded { get; }

			long Remaining { get; }

			long Size { get; }

			double Relative { get; }

			int LastBlockSize { get; }

			long Speed { get; }

		    event EventHandler ProgressUpdate;
		}

		private class ProgressInfo : IProgressInfo
		{
			[DoNotNotify]
			public long Downloaded { get; set; }

			[DoNotNotify]
			public long Remaining => this.Size > 0 ? (this.Size - this.Downloaded) : 0;

			[DoNotNotify]
			public long Size { get; set; }

			[DoNotNotify]
			public double Relative => this.Size > 0 ? ((double)this.Downloaded / this.Size) : 0;

			[DoNotNotify]
			public int LastBlockSize { get; set; }

			[DoNotNotify]
			public long Speed { get; set; }

		    public event EventHandler ProgressUpdate;

		    public void OnProgressUpdate()
		    {
		        ProgressUpdate?.BeginInvoke(this, new EventArgs(), ar => ProgressUpdate?.EndInvoke(ar), null);
		    }
		}
		

		public event EventHandler<DownloadConnectionEstablishedEventArgs> ConnectionEstablished;
		public event EventHandler<BlockDownloadCompletedEventArgs> BlockDownloadCompleted;
		public event EventHandler<DownloadCompletedEventArgs> DownloadCompleted;
		public event EventHandler<DownloadExceptionEventArgs> DownloadException;

		public BackgroundDownloadState State { get; private set; } = BackgroundDownloadState.Waiting;
		private int connectTimeout = 30000;
		public int ConnectTimeout
		{
			get => this.connectTimeout;
			set
			{
				if (value < -1)
					throw new ArgumentOutOfRangeException(nameof(value));
				CheckIfAlreadyRunning();
				this.connectTimeout = value;
			}
		}

		private int blockDownloadTimeout = 30000;
		public int BlockDownloadTimeout
		{
			get => this.blockDownloadTimeout;
			set
			{
				if (value < -1)
					throw new ArgumentOutOfRangeException(nameof(value));
				CheckIfAlreadyRunning();
				this.blockDownloadTimeout = value;
			}
		}

		private int blockSize = 1024;
		public int BlockSize
		{
			get => this.blockSize;
			set
			{
				if (value < 0)
					throw new ArgumentOutOfRangeException(nameof(value));

				CheckIfAlreadyRunning();
				this.blockSize = value;
			}
		}

		private int progressUpdateTime = 1000;
		public int ProgressUpdateTime
		{
			get => this.progressUpdateTime;
			set
			{
				if (value < 0)
					throw new ArgumentOutOfRangeException(nameof(value));
				this.progressUpdateTime = value;
			}
		}

		public bool Pause
		{
			get => !unpausedEvent.WaitOne(0);
			set
			{
				if (value)
					this.unpausedEvent.Reset();
				else
					this.unpausedEvent.Set();
			}
		}

		private Stream outputStream = new MemoryStream();
		public Stream OutputStream
		{
			get => this.outputStream;
			set
			{
				if (value == null)
					throw new ArgumentNullException(nameof(value));

				CheckIfAlreadyRunning();
				this.outputStream = value;
			}
		}

		public bool CloseStreamOnFinish { get; set; } = true;

		private readonly ProgressInfo progress = new ProgressInfo();
		public IProgressInfo Progress => progress;

		public WaitHandle WaitHandle => finishedEvent;

		private readonly ManualResetEvent unpausedEvent = new ManualResetEvent(true);
		private readonly ManualResetEvent startedEvent = new ManualResetEvent(false);
		private readonly ManualResetEvent finishedEvent = new ManualResetEvent(false);

		private Func<HttpWebRequest, HttpWebResponse, HttpWebRequest> repeatRequestCallback = null;
		public Func<HttpWebRequest, HttpWebResponse, HttpWebRequest> RepeatRequestCallback
		{
			get => this.repeatRequestCallback;
			set
			{
				CheckIfAlreadyRunning();
				this.repeatRequestCallback = value;
			}
		}

		private HttpWebRequest request;
		private HttpWebResponse response;
		private Stream responseStream;
		private byte[] downloadBuffer;
		
		public DateTime? StartTime { get; private set; }
		public DateTime? FinishTime { get; private set; }

		private RegisteredWaitHandle currentActionWaitHandle;

		public BackgroundDownload(Uri url)
		{
			this.request = (HttpWebRequest) WebRequest.Create(url);
			this.request.Method = "GET";
		}
		public BackgroundDownload(HttpWebRequest request)
		{
			this.request = request;
			
		}

		private void CheckIfAlreadyRunning()
		{
			if (this.State != BackgroundDownloadState.Waiting)
				throw new InvalidOperationException("Can't change settings of a already running download.");
		}


	    /*public Task Execute()
	    {
	        if (this.State != BackgroundDownloadState.Waiting)
                throw new InvalidOperationException();

	        return Task.Run(() =>
	        {
	            startedEvent.Set();
	            ThreadPool.QueueUserWorkItem(this.UpdateDownloadSpeed);

	            this.StartTime = DateTime.Now;
	            this.State = BackgroundDownloadState.Connecting;

	            this.request.ReadWriteTimeout = this.BlockDownloadTimeout;
	            this.request.Timeout = ConnectTimeout;

	            this.response = (HttpWebResponse) this.request.GetResponse();

	            if (response.StatusCode != HttpStatusCode.OK)
	            {
	                if (this.RepeatRequestCallback != null)
	                    this.RepeatRequest(this.RepeatRequestCallback(this.request, this.response));
	                else
	                    this.Abort();
	                return;

	            }

	            this.OnConnectionEstablished(new DownloadConnectionEstablishedEventArgs(response));
	            this.State = BackgroundDownloadState.Downloading;

	            this.responseStream = this.response.GetResponseStream();
	            this.responseStream.ReadTimeout = this.BlockDownloadTimeout;

	            this.downloadBuffer = new byte[this.BlockSize];

	            this.progress.Downloaded = 0;
	            if (response.ContentLength >= 0)
	                this.progress.Size = response.ContentLength;
                
	            this.unpausedEvent.WaitOne();
	            int read = this.responseStream.Read(this.downloadBuffer, 0, this.BlockSize);

                while(read > 0)
	            {
	                this.progress.Downloaded += read;
	                this.progress.LastBlockSize = read;

	                this.OutputStream.Write(this.downloadBuffer, 0, read);

	                this.unpausedEvent.WaitOne();
	                read = this.responseStream.Read(this.downloadBuffer, 0, this.BlockSize);
                    
	            }

	            if (this.progress.Remaining == 0)
	            {
	                this.FinishTime = DateTime.Now;

	                this.response?.Close();
	                if (this.CloseStreamOnFinish)
	                    this.OutputStream.Close();
	                this.State = BackgroundDownloadState.Successful;

	                this.finishedEvent.Set();
	            }
	            else
	            {
	                this.response?.Close();
	                if (this.CloseStreamOnFinish)
	                    this.OutputStream.Close();

	                this.State = BackgroundDownloadState.Aborted;
	                this.finishedEvent.Set();
                    throw new DownloadIncompleteException();
	            }
            });
	    }*/

		public void Start()
		{
			if (this.State != BackgroundDownloadState.Waiting)
				return;

			startedEvent.Set();
			ThreadPool.QueueUserWorkItem(this.UpdateDownloadSpeed);

			this.StartTime = DateTime.Now;
			this.State = BackgroundDownloadState.Connecting;

			this.request.ReadWriteTimeout = this.BlockDownloadTimeout;
			this.request.Timeout = ConnectTimeout;

			var ar = this.request.BeginGetResponse(this.GetReponseCallback, null);
			this.currentActionWaitHandle = ThreadPool.RegisterWaitForSingleObject(ar.AsyncWaitHandle, GetReponseTimeoutCallback, null, (uint)this.BlockDownloadTimeout, true);
		}

		public void Abort()
		{
			this.FinishTime = DateTime.Now;
			
			this.State = BackgroundDownloadState.Aborted;
			this.request.Abort();
			response?.Close();
			this.finishedEvent.Set();
		}

		private void UpdateDownloadSpeed(object state)
		{
			long doneLastTime = 0;
			while (!this.finishedEvent.WaitOne(this.ProgressUpdateTime))
			{
				long doneNow = this.Progress.Downloaded - doneLastTime;
				doneLastTime = this.Progress.Downloaded;
				this.progress.Speed = doneNow * 1000 / ProgressUpdateTime;
			    this.progress.OnProgressUpdate();
			}
		}

		private void RepeatRequest(HttpWebRequest newRequest)
		{
			this.request.Abort();
			response?.Close();
			this.response = null;
			this.responseStream = null;

			if (newRequest == null)
			{
				this.Abort();
				return;
			}

			this.request = newRequest;
			this.request.ReadWriteTimeout = this.BlockDownloadTimeout;
			this.request.Timeout = ConnectTimeout;

			var ar = this.request.BeginGetResponse(this.GetReponseCallback, null);
			this.currentActionWaitHandle = ThreadPool.RegisterWaitForSingleObject(ar.AsyncWaitHandle, GetReponseTimeoutCallback, null, (uint)this.BlockDownloadTimeout, true);
		}

		private void GetReponseCallback(IAsyncResult ar)
		{
			this.response = (HttpWebResponse) this.request.EndGetResponse(ar);
			this.currentActionWaitHandle.Unregister(ar.AsyncWaitHandle);

			if (response.StatusCode != HttpStatusCode.OK)
			{
				if (this.RepeatRequestCallback != null)
					this.RepeatRequest(this.RepeatRequestCallback(this.request, this.response));
				else
					this.Abort();
				return;

			}

			this.OnConnectionEstablished(new DownloadConnectionEstablishedEventArgs(response));
			this.State = BackgroundDownloadState.Downloading;

			this.responseStream = this.response.GetResponseStream();
			this.responseStream.ReadTimeout = this.BlockDownloadTimeout;

			this.downloadBuffer = new byte[this.BlockSize];

			this.progress.Downloaded = 0;
			if (response.ContentLength >= 0)
				this.progress.Size = response.ContentLength;

			Console.WriteLine("Starting read loop");
			this.unpausedEvent.WaitOne();

			var rar = this.responseStream.BeginRead(this.downloadBuffer, 0, this.BlockSize, this.ReadCallback, null);
			this.currentActionWaitHandle = ThreadPool.RegisterWaitForSingleObject(rar.AsyncWaitHandle, BlockDownloadTimeoutCallback, null, (uint)this.BlockDownloadTimeout, true);
		}

		private void GetReponseTimeoutCallback(object state, bool timedOut)
		{
			if (timedOut)
			{
				this.Abort();
			}
		}

		private void ReadCallback(IAsyncResult ar)
		{
			int read = this.responseStream.EndRead(ar);
			this.currentActionWaitHandle.Unregister(ar.AsyncWaitHandle);
			
			this.progress.Downloaded += read;
			this.progress.LastBlockSize = read;

			if (read > 0)
			{
				this.OutputStream.Write(this.downloadBuffer, 0, read);

				this.OnBlockDownloadCompleted(new BlockDownloadCompletedEventArgs(
					read, this.progress.Downloaded, this.progress.Remaining, this.progress.Size
					));

				this.unpausedEvent.WaitOne();
				var rar = this.responseStream.BeginRead(this.downloadBuffer, 0, this.BlockSize, this.ReadCallback, null);
				this.currentActionWaitHandle = ThreadPool.RegisterWaitForSingleObject(rar.AsyncWaitHandle, BlockDownloadTimeoutCallback, null, (uint)this.BlockDownloadTimeout, true);

			}
			else if (this.progress.Remaining == 0)
			{
				this.FinishTime = DateTime.Now;

				this.response?.Close();
				if (this.CloseStreamOnFinish)
					this.OutputStream.Close();
				this.State = BackgroundDownloadState.Completed;
				OnDownloadCompleted(new DownloadCompletedEventArgs(this.progress.Downloaded));
				this.finishedEvent.Set();
			}
			else
			{
				this.response?.Close();
				if (this.CloseStreamOnFinish)
					this.OutputStream.Close();

				this.State = BackgroundDownloadState.Aborted;
				OnDownloadException(new DownloadExceptionEventArgs(new DownloadIncompleteException()));
				this.finishedEvent.Set();
			}
		}
		

		private void BlockDownloadTimeoutCallback(object state, bool timedOut)
		{
			if (timedOut)
			{
				this.Abort();
			}
		}

		protected void OnConnectionEstablished(DownloadConnectionEstablishedEventArgs e)
		{
			ConnectionEstablished?.BeginInvoke(this, e, ar => ConnectionEstablished?.EndInvoke(ar), null);
		}

		protected void OnBlockDownloadCompleted(BlockDownloadCompletedEventArgs e)
		{
			BlockDownloadCompleted?.BeginInvoke(this, e, ar => BlockDownloadCompleted?.EndInvoke(ar), null);
		}

		protected void OnDownloadCompleted(DownloadCompletedEventArgs e)
		{
			DownloadCompleted?.BeginInvoke(this, e, ar => DownloadCompleted?.EndInvoke(ar), null);
		}

		protected void OnDownloadException(DownloadExceptionEventArgs e)
		{
			DownloadException?.BeginInvoke(this, e, ar => DownloadException?.EndInvoke(ar), null);
		}


		public event PropertyChangedEventHandler PropertyChanged;

		protected void OnPropertyChanged(PropertyChangedEventArgs e)
		{
			PropertyChanged?.Invoke(this, e);
		}
	}
}
