﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CFAN.Factorio.Api;
using CFAN.Tasks;
using CFAN.Tasks.Waiting;

namespace CFAN.Downloads
{
	public class LocalModFile : LocalFile
	{
		public ModInfoBase Mod { get; }
		public Version Version { get; }

		public LocalModFile(string filePath, ModInfoBase mod, Version version)
            : base(filePath)
		{
			Mod = mod;
			Version = version;
		}

        public LocalModFile(string filePath, BackgroundDownload runningDownload, string runningDownloadFilePath, 
            ModInfoBase mod, Version version)
            : base(filePath, runningDownload, runningDownloadFilePath)
        {
            Mod = mod;
            Version = version;
        }

	    public Task InstallTo(string path, CompositeTask containingTask = null)
	    {
	        var tOverall = new AsyncTask($"Installing Mod {this.Mod.Title}");
	        containingTask?.SubTasks.Add(tOverall);

	        var task = this.Await().ContinueWith((prev) =>
	        {
	            if (prev.IsCanceled || prev.IsFaulted)
	                return;

	            if (!Directory.Exists(path))
	                Directory.CreateDirectory(path);

	            var sourceFile = this.FilePath;
	            var fileName = Path.GetFileName(sourceFile);
	            var targetFile = Path.Combine(path, fileName);
                try
                {
                    File.Copy(sourceFile, targetFile);
                }
                catch (FileNotFoundException)
                {
                    tOverall.ManualMarkFailed();
                }
            });
	        tOverall.Task = task;
	        return task;
	    }
    }
}
