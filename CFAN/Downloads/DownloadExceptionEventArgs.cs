﻿using System;
using System.Runtime.ExceptionServices;

namespace CFAN.Downloads
{
	public class DownloadExceptionEventArgs : EventArgs
	{
		public DownloadExceptionEventArgs(Exception exception)
		{
			Exception = exception;
		}

		public Exception Exception { get; }

		public void Throw()
		{
			ExceptionDispatchInfo.Capture(this.Exception).Throw();
		}
	}
}
