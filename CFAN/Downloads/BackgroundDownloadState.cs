﻿namespace CFAN.Downloads
{
	public enum BackgroundDownloadState
	{
		Waiting,
		Connecting,
		Downloading,
		Completed,
		Aborted

	}

}
