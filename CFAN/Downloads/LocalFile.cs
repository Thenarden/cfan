﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using CFAN.Tasks.Waiting;
using CFAN.Utils;
using Shortbit.Utils;

namespace CFAN.Downloads
{
	public class LocalFile : IWaitable
	{

		public BackgroundDownload RunningDownload { get; private set; }

	    public string RunningDownloadFilePath { get; private set; }

	    public string FilePath { get; }

	    public bool IsFile { get; }

        public bool IsFinished =>
			this.RunningDownload == null || this.RunningDownload.State == BackgroundDownloadState.Completed;

        private ManualResetEvent fileCopied = new ManualResetEvent(false);


	    protected LocalFile(string filePath)
	    {
	        Throw.IfNullOrWhitespace(filePath, nameof(filePath));

            this.FilePath = filePath;
	        this.IsFile = FileUtils.IsArchiveFile(this.FilePath);
		    this.RunningDownload = null;
		    this.RunningDownloadFilePath = null;

	    }
	    protected LocalFile(string filePath, BackgroundDownload runningDownload, string runningDownloadFilePath)
	    {
            Throw.IfNullOrWhitespace(filePath, nameof(filePath));
	        Throw.IfNull(runningDownload, nameof(runningDownload));
	        Throw.IfNullOrWhitespace(runningDownloadFilePath, nameof(runningDownloadFilePath));

            this.FilePath = filePath;
	        this.IsFile = FileUtils.IsArchiveFile(this.FilePath);
            this.RunningDownload = runningDownload;
	        this.RunningDownloadFilePath = runningDownloadFilePath;

	        this.RunningDownload.DownloadCompleted += (sender, args) =>
	        {
	            if (!Directory.Exists(Path.GetDirectoryName(this.FilePath)))
	                Directory.CreateDirectory(Path.GetDirectoryName(this.FilePath));

                if (File.Exists(this.FilePath))
                    File.Delete(this.FilePath);

                using (FileStream sourceStream = File.Open(this.RunningDownloadFilePath, FileMode.Open))
                {
                    using (FileStream destinationStream = File.Create(this.FilePath))
                    {
                        sourceStream.CopyTo(destinationStream);
                        sourceStream.Close();
                    }
                }

                File.Delete(this.RunningDownloadFilePath);
                this.RunningDownload = null;
	            this.RunningDownloadFilePath = null;
                this.fileCopied.Set();

            };

	    }

        
	    public bool WaitOne(int millisecondsTimeout)
	    {
	        if (this.IsFinished)
	            return true;
            return this.fileCopied.WaitOne(millisecondsTimeout);
        }
        
	}
}
