﻿using System;
using System.Net;

namespace CFAN.Downloads
{
	public class DownloadConnectionEstablishedEventArgs : EventArgs
	{
		public DownloadConnectionEstablishedEventArgs(HttpWebResponse reponse)
		{
			Reponse = reponse;
		}

		public HttpWebResponse Reponse { get; }
	}
}
