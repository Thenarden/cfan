﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CFAN.Factorio.Api;
using CFAN.Model;
using CFAN.Tasks;
using CFAN.Tasks.Waiting;

namespace CFAN.Downloads
{
	public class LocalGameVersionFile : LocalFile
	{
		public GameVersion GameVersion { get; }
		public Platform Platform { get; }


		public LocalGameVersionFile(string filePath, GameVersion gameVersion, Platform platform)
            : base(filePath)
		{
			GameVersion = gameVersion;
			Platform = platform;
		}

        public LocalGameVersionFile(string filePath, BackgroundDownload runningDownload, string runningDownloadFilePath, 
            GameVersion gameVersion, Platform platform)
            : base(filePath, runningDownload, runningDownloadFilePath)
        {
            GameVersion = gameVersion;
            Platform = platform;
        }

	    public Task InstallTo(string path, CompositeTask containingTask = null)
	    {
	        var tOverall = new AsyncTask($"Installing Factorio {this.GameVersion.MainVersion}");
	        containingTask?.SubTasks.Add(tOverall);

	        var skippedNamePart = this.GameVersion.GetArchiveRootFolderName().Length + 1;
        
	        var task = this.Await().ContinueWith((prev) =>
	        {
	            if (prev.IsCanceled || prev.IsFaulted)
	                return;

	            if (!Directory.Exists(path))
	                Directory.CreateDirectory(path);

	            var file = this.FilePath;

	            using (var stream = new FileStream(file, FileMode.Open))
	            using (var archive = new ZipArchive(stream))
	            {

	                var progress = new Progress<int>();
	                tOverall.SetProgress(progress, archive.Entries.Count);
	                var reporter = (IProgress<int>) progress;

	                int i = 1;
	                foreach (var entry in archive.Entries)
	                {
	                    reporter.Report(i);
	                    i++;
	                    var relName = entry.FullName.Substring(skippedNamePart);
	                    var targetName = Path.Combine(path, relName);

	                    var dirName = Path.GetDirectoryName(targetName);
	                    if (!Directory.Exists(dirName))
	                        Directory.CreateDirectory(dirName);

	                    using (var targetStream = new FileStream(targetName, FileMode.Create))
	                    using (var sourceStream = entry.Open())
	                    {
	                        sourceStream.CopyTo(targetStream);
	                    }
	                }
	            }

	        });
	        tOverall.Task = task;
	        return task;
	    }
    }
}
