﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CFAN.Exceptions
{
	public class NotAModException : Exception
	{
		public NotAModException()
		{
		}

		public NotAModException(string message) : base(message)
		{
		}

		public NotAModException(string message, Exception innerException) : base(message, innerException)
		{
		}

		protected NotAModException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
