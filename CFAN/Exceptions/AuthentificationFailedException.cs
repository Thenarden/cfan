﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CFAN.Exceptions
{
	public class AuthentificationFailedException : Exception
	{
		public AuthentificationFailedException()
		{
		}

		public AuthentificationFailedException(string message) : base(message)
		{
		}

		public AuthentificationFailedException(string message, Exception innerException) : base(message, innerException)
		{
		}

		protected AuthentificationFailedException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
