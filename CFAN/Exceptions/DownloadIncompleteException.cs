﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CFAN.Exceptions
{
	public class DownloadIncompleteException : Exception
	{
		public DownloadIncompleteException()
		{
		}

		public DownloadIncompleteException(string message) : base(message)
		{
		}

		public DownloadIncompleteException(string message, Exception innerException) : base(message, innerException)
		{
		}

		protected DownloadIncompleteException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
