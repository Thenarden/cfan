﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CFAN.Downloads;
using CFAN.Factorio.Api;
using CFAN.Model;
using CFAN.Utils;
using Shortbit.Utils.Collections.Generic;

namespace CFAN.Context
{
	public class DownloadManager
	{
		public AppContext Context { get; }
		
		private readonly BiDictionary<Tuple<GameVersion, Platform>, LocalGameVersionFile> gameVersionDownloads = new BiDictionary<Tuple<GameVersion, Platform>, LocalGameVersionFile>();
		private readonly BiDictionary<Tuple<string, Version>, LocalModFile> modDownloads = new BiDictionary<Tuple<string, Version>, LocalModFile>();

		private readonly LinkedList<BackgroundDownload> queuedDownloads = new LinkedList<BackgroundDownload>();
		private readonly List<BackgroundDownload> runningDownloads = new List<BackgroundDownload>();
		private readonly List<BackgroundDownload> finishedDownloads = new List<BackgroundDownload>();

		public IEnumerable<BackgroundDownload> QueuedDownloads => queuedDownloads;
		public IEnumerable<BackgroundDownload> RunningDownloads => runningDownloads;
		public IEnumerable<BackgroundDownload> FinishedDownloads => finishedDownloads;

		public WaitHandle AllFinishedHandle => allFinished;

		private int consecutiveDownloads = 1;
		public int ConsecutiveDownloads {
			get => this.consecutiveDownloads;
			set
			{
				if (value < 1)
					throw new ArgumentOutOfRangeException(nameof(value));
				this.consecutiveDownloads = value;
			}
			
		}

		private readonly ManualResetEvent terminate = new ManualResetEvent(false);
		private readonly ManualResetEvent allFinished = new ManualResetEvent(true);

		private Thread TStartNew;

		public DownloadManager(AppContext context)
		{
			this.Context = context;

			this.TStartNew = new Thread(this.TStartNew_Run)
			{
				IsBackground = true,
				Name = "DownloadManager-Update" 
			};
			this.TStartNew.Start();
		}


		private void QueuedDownloadStateChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
		{
			if (propertyChangedEventArgs.PropertyName != nameof(BackgroundDownload.State))
				return;
			var download = (BackgroundDownload) sender;

			if (download.State == BackgroundDownloadState.Connecting ||
			     download.State == BackgroundDownloadState.Downloading)
			{
				queuedDownloads.Remove(download);
				this.StartDownload(download);
			}
			else if (download.State == BackgroundDownloadState.Aborted)
				this.RemoveDownload(download, false);

		}
		private void RunningDownloadStateChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
		{
			if (propertyChangedEventArgs.PropertyName != nameof(BackgroundDownload.State))
				return;
			var download = (BackgroundDownload)sender;

			if (download.State == BackgroundDownloadState.Completed ||
			    download.State == BackgroundDownloadState.Aborted)
			{
				this.RemoveDownload(download, true);
			}

		}

		private void EnqueueDownload(BackgroundDownload download)
		{
			download.PropertyChanged += QueuedDownloadStateChanged;
			this.queuedDownloads.AddLast(download);
			allFinished.Reset();
		}
		private void StartDownload(BackgroundDownload download)
		{
			download.PropertyChanged -= QueuedDownloadStateChanged;

			runningDownloads.Add(download);
			download.Start();

			download.PropertyChanged += RunningDownloadStateChanged;
		}
		private void RemoveDownload(BackgroundDownload download, bool wasStarted)
		{
			if (wasStarted)
			{
				download.PropertyChanged -= RunningDownloadStateChanged;
				runningDownloads.Remove(download);
			}
			else
			{
				download.PropertyChanged -= QueuedDownloadStateChanged;
				queuedDownloads.Remove(download);
			}

			finishedDownloads.Add(download);

			if (!runningDownloads.Any() && !queuedDownloads.Any())
			{
				allFinished.Set();
			}
		}

		private void TStartNew_Run()
		{
			while (!this.terminate.WaitOne(10))
			{
				if (this.allFinished.WaitOne(0))
					continue;

				if (this.runningDownloads.Count < this.ConsecutiveDownloads && this.queuedDownloads.Any())
				{
					var nextDownload = this.queuedDownloads.First.Value;
					this.queuedDownloads.RemoveFirst();
					this.StartDownload(nextDownload);
				}
			}
		}

        
        public LocalModFile DownloadMod(ModInfoBase modInfo, Version version)
		{
			if (this.Context.Cache.CacheContainsMod(modInfo, version))
				return this.Context.Cache.GetCachedMod(modInfo, version);

		    var id = Tuple.Create(modInfo.Name, version);

		    if (modDownloads.TryGetByFirst(id, out var localFile))
		        return localFile;

            var targetFile = this.Context.Cache.CacheModFile(modInfo, version);
		    var downloadFile = targetFile + ".part";


		    var adapter = modInfo.Adapter;
		    var download = adapter.DownloadMod(modInfo, version);

		    if (!Directory.Exists(this.Context.Cache.ModsDownloadFolder))
		        Directory.CreateDirectory(this.Context.Cache.ModsDownloadFolder);

		    download.OutputStream = new FileStream(downloadFile, FileMode.Create);
		    download.CloseStreamOnFinish = true;

            localFile = new LocalModFile(targetFile, download, downloadFile, modInfo, version);

		    modDownloads.Add(id, localFile);
		    this.EnqueueDownload(download);
		    return localFile;
        }
        
		public LocalGameVersionFile DownloadGameVersion(GameVersion gameVersion, Platform platform)
		{
		    if (this.Context.Cache.CacheContainsGameVersion(gameVersion, platform))
		        return this.Context.Cache.GetCachedGameVersion(gameVersion, platform);

		    var id = Tuple.Create(gameVersion, platform);

		    if (gameVersionDownloads.TryGetByFirst(id, out var localFile))
		        return localFile;

		    var targetFile = this.Context.Cache.CacheGameVersionFile(gameVersion, platform);
		    var downloadFile = targetFile + ".part";

            var adapter = gameVersion.Adapter;
		    var download = adapter.DownloadGameVersion(gameVersion, platform);
		    if (!Directory.Exists(this.Context.Cache.GameVersionsDownloadFolder))
		        Directory.CreateDirectory(this.Context.Cache.GameVersionsDownloadFolder);
            download.OutputStream = new FileStream(downloadFile, FileMode.Create);

            localFile = new LocalGameVersionFile(targetFile, download, downloadFile, gameVersion, platform);

		    gameVersionDownloads.Add(id, localFile);
		    this.EnqueueDownload(download);
		    return localFile;
        }
        
	}
}
