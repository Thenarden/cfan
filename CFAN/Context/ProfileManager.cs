﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CFAN.Downloads;
using CFAN.Factorio.Api;
using CFAN.Model;
using CFAN.Tasks;
using CFAN.Tasks.Waiting;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Shortbit.Utils;
using Shortbit.Utils.Collections.ObjectModel;

namespace CFAN.Context
{
	public class ProfileManager : IList<Profile>, INotifyCollectionChanged, INotifyPropertyChanged
	{
		public static string ProfilesFile => Path.Combine(AppSettings.AppDataFolder, "profiles.json");


		public Profile this[int index]
		{
			get => this.profiles[index];
			set => this.profiles[index] = value;
		}

		public Profile this[Guid id]
		{
			get => this.profiles[this.localIdMap[id]];
		}

		public int Count => this.profiles.Count;
		public bool IsReadOnly => (this.profiles as ICollection<Profile>).IsReadOnly;


		private readonly AsyncObservableCollection<Profile> profiles;
		private readonly Dictionary<Guid, int> localIdMap;

		private readonly AppContext context;

		private readonly JsonSerializerSettings jsonSettings = new JsonSerializerSettings
		{
			Formatting = Formatting.Indented,
			Converters = new List<JsonConverter> {new VersionConverter()}
		};

		public ProfileManager(AppContext context)
		{
			this.context = context;

			var existing = new List<Profile>();
			this.localIdMap = new Dictionary<Guid, int>();

			bool changed = false;

			if (File.Exists(ProfilesFile))
			{
				existing = JsonConvert.DeserializeObject<List<Profile>>(File.ReadAllText(ProfilesFile), jsonSettings);
				var remove = new List<Profile>();

				for (int i = 0; i < existing.Count; i++)
				{
					var profile = existing[i];
					
					if (string.IsNullOrWhiteSpace(profile.ProfileFolder))
						profile.ProfileFolder = Path.Combine(context.Settings.DefaultProfileFolder, profile.Name);

					if (!Directory.Exists(profile.ProfileFolder)
						|| profile.InProgress)
						remove.Add(profile);

					this.localIdMap.Add(profile.LocalId, i);
				}

				changed = changed || remove.Any();

				foreach (var p in remove)
					existing.Remove(p);
			}

			this.profiles = new AsyncObservableCollection<Profile>(existing);
			this.profiles.CollectionChanged += ProfilesOnCollectionChanged;
			foreach (var p in profiles)
			{
				p.PropertyChanged += ProfileOnPropertyChanged;
			}

			if (changed)
				this.Save();
			
		}

		private void Save()
		{
			var p = this.profiles.ToList();
			File.WriteAllText(ProfilesFile, JsonConvert.SerializeObject(p, jsonSettings));
		}

		private void ProfileOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
		{
			this.Save();
		}

		private void ProfilesOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs args)
		{
			this.Save();

			/*if (args.Action == NotifyCollectionChangedAction.Remove || args.Action == NotifyCollectionChangedAction.Replace || args.Action == NotifyCollectionChangedAction.Reset)
				foreach (ProfileView p in args.OldItems)
					p.PropertyChanged -= this.ProfileOnPropertyChanged;
			if (args.Action == NotifyCollectionChangedAction.Add || args.Action == NotifyCollectionChangedAction.Replace)
				foreach (ProfileView p in args.NewItems)
					p.PropertyChanged += this.ProfileOnPropertyChanged;*/
		}


		public IEnumerable<Profile> GetById(Guid id)
		{
			return this.profiles.Where(p => p.Id == id);
		}
        

		public async Task<Profile> Create(string name, string path, GameVersion gameVersion, CompositeTask containingTask = null)
		{
		    var tOverall = new CompositeTask(2, $"Creating profile {name}");
            tOverall.AddTo(containingTask, this.context.Tasks);

			var profile = new Profile(name, path);
			profile.GameVersion = gameVersion.Version;
			profile.InProgress = true;

			var localFile = this.context.Downloads.DownloadGameVersion(gameVersion, PlatformInfo.CurrentPlatform);
            tOverall.SubTasks.Add(new DownloadTask(localFile));

			Directory.CreateDirectory(profile.ProfileFolder);

			this.Add(profile);
            
			await localFile.InstallTo(path, tOverall);
			profile.InProgress = false;

			return profile;
		}

		public void Delete(Profile profile)
		{
			if (!this.Contains(profile))
				return;

			if (Directory.Exists(profile.ProfileFolder))
				Directory.Delete(profile.ProfileFolder, true);
			this.Remove(profile);
		}


		public void Export(Profile profile, string outputFile, bool minimalistic, string name = null)
		{
			if (string.IsNullOrWhiteSpace(name))
				name = profile.Name;

			var includedMods = profile.Mods.Mods.ToList();
			var downloadedMods = new List<Mod>();

			var manifest = new ProfileManifest
			{
				Id = profile.Id,
				Name = name,
				Author = profile.Author,
				Description = profile.Description,
				GameVersion = profile.GameVersion,
				ProfileVersion = profile.ProfileVersion,
				DownloadedMods = downloadedMods.Select(m => m.GetId()).ToList(),
				IncludedMods = includedMods.Select(m => m.GetId()).ToList()
			};


			using (var stream = new FileStream(outputFile, FileMode.Create))
			using (var archive = new ZipArchive(stream, ZipArchiveMode.Create))
			{

				if (File.Exists(profile.FullIconFile))
				{
					archive.CreateEntry(Path.GetFileName(profile.IconFile)).CopyFromFile(profile.FullIconFile);
					manifest.IconFile = Path.GetFileName(profile.IconFile);
				}

				var manifestEntry = archive.CreateEntry("manifest.json");
				manifestEntry.SetContent(JsonConvert.SerializeObject(manifest,
					new JsonSerializerSettings
					{
						Formatting = Formatting.Indented,
						Converters = new List<JsonConverter>
						{
							new VersionConverter()
						}
					}));


				if (File.Exists(Path.Combine(profile.ProfileFolder, @"mods\mod-list.json")))
					archive.CreateEntry(@"mods\mod-list.json")
						.CopyFromFile(Path.Combine(profile.ProfileFolder, @"mods\mod-list.json"));

				if (File.Exists(Path.Combine(profile.ProfileFolder, @"mods\mod-settings.json")))
					archive.CreateEntry(@"mods\mod-settings.json")
						.CopyFromFile(Path.Combine(profile.ProfileFolder, @"mods\mod-settings.json"));

				foreach (var mod in includedMods)
				{
					var pathname = Path.GetFileName(mod.Path);

					if (mod.IsArchive)
					{
						archive.CreateEntry(@"mods\"+ pathname).CopyFromFile(mod.Path);
					}
					else
					{
						foreach (string newPath in Directory.GetFiles(mod.Path, "*.*",
							SearchOption.AllDirectories))
						{
							var relPath = newPath.Replace(mod.Path, "");

							archive.CreateEntry(@"mods\" + pathname + relPath).CopyFromFile(newPath);

						}

					}
				}

			}
			
			
		}

		public ProfileManifest GetManifestFromArchive(string path)
		{
			Throw.IfNullOrWhitespace(path, nameof(path));
			if (!File.Exists(path))
				throw new ArgumentException();

			string manifestContent;
			using (var stream = new FileStream(path, FileMode.Open))
			using (var archive = new ZipArchive(stream))
			{
				manifestContent = archive.GetEntry("manifest.json")?.GetContent();
			}

			if (manifestContent == null)
				throw new ArgumentException();

			var manifest = JsonConvert.DeserializeObject<ProfileManifest>(manifestContent);
			return manifest;
		}

		public Guid GetProfileIdFromArchive(string path)
		{
			var manifest = this.GetManifestFromArchive(path);
			return manifest.Id;
		}

		public Task<Profile> CreateFromExport(string inputFile, string newName, string path = null, CompositeTask containingTask = null)
        {
            return Task.Run(() =>
            {
                Throw.IfNullOrWhitespace(newName, nameof(newName));

                var tOverall = new CompositeTask(5, $"Importing profile {newName}");
                tOverall.AddTo(containingTask, this.context.Tasks);

                var manifest = this.GetManifestFromArchive(inputFile);

                if (string.IsNullOrWhiteSpace(path))
                    path = Path.Combine(this.context.Settings.DefaultProfileFolder, newName);

                var gameversionTask = Task.Run(() =>
                {
                    var availableVersions = this.context.GameVersionAdapter.ListGameVersions().ToListSafe();

                    var res = availableVersions.FirstOrDefault(gv => gv.Version == manifest.GameVersion);
                    if (res == null)
                        res = availableVersions.FirstOrDefault(gv => gv.MainVersion == manifest.MainGameVersion);
                    if (res == null)
                        throw new InvalidOperationException($"Can't find Game Version {manifest.GameVersion}");
                    return res;
                });
                tOverall.SubTasks.Add(new AsyncTask(gameversionTask, "Fetching game versions"));


                var gameversion = gameversionTask.Result;
                var localFile = this.context.Downloads.DownloadGameVersion(gameversion, PlatformInfo.CurrentPlatform);
                tOverall.SubTasks.Add(new DownloadTask(localFile));

                var profile = new Profile(newName, path, manifest.Id)
                {
                    Author = manifest.Author,
                    Description = manifest.Description,
                    GameVersion = gameversion.Version,
                    ProfileVersion = manifest.ProfileVersion,

                    InProgress = true,
                };

                this.Add(profile);

                if (Directory.Exists(path))
                    Directory.Delete(path, true);
                Directory.CreateDirectory(path);

                localFile.InstallTo(profile.ProfileFolder, tOverall).Wait();

                if (!Directory.Exists(Path.Combine(profile.ProfileFolder, "mods")))
                    Directory.CreateDirectory(Path.Combine(profile.ProfileFolder, "mods"));

                var tModsSetup = new CompositeTask(manifest.DownloadedMods.Count + manifest.IncludedMods.Count,
                                                   $"Installing mods");
                tOverall.SubTasks.Add(tModsSetup);
                foreach (var mod in manifest.DownloadedMods)
                {
                    var modInfo = this.context.ModInfoAdapter.GetModInfo(mod.Name);
                    if (modInfo == null)
                        throw new InvalidOperationException($"Can't find Mod {mod.Name}");

                    this.context.Mods.InstallMod(profile, modInfo, mod.Version, true, tModsSetup).Wait();
                }

                using (var stream = new FileStream(inputFile, FileMode.Open))
                using (var archive = new ZipArchive(stream))
                {
                    foreach (var mod in manifest.IncludedMods)
                    {
                        var tMod = new CompositeTask(profile.Mods.Contains(mod.Name) ? 2 : 1,
                                                     $"Installing bundled mod {mod.Name}");
                        tModsSetup.SubTasks.Add(tMod);
                        if (profile.Mods.Contains(mod.Name))
                            this.context.Mods.UninstallMod(profile, mod.Name, tMod).Wait();


                        if (mod.IsArchive)
                        {
                            var progress = new Progress<int>();
                            var tModSetup = new AsyncTask(progress, 1, "Copying archive");
                            tMod.SubTasks.Add(tModSetup);

                            archive.GetEntry(@"mods\" + mod.PathName)
                                   .CopyToFile(Path.Combine(profile.ProfileFolder, @"mods\" + mod.PathName));
                            ((IProgress<int>) progress).Report(1);
                        }
                        else
                        {

                            var modFolder = Path.Combine(profile.ProfileFolder, @"mods\" + mod.PathName);

                            if (!Directory.Exists(modFolder))
                                Directory.CreateDirectory(modFolder);

                            var entries = archive.EntriesInFolder(@"mods\" + mod.PathName).ToListSafe();

                            var progress = new Progress<int>();
                            var tModSetup = new AsyncTask(progress, entries.Count, "Copying folder content");
                            tMod.SubTasks.Add(tModSetup);

                            int i = 1;
                            foreach (var entry in entries)
                            {
                                var targetFile = Path.Combine(profile.ProfileFolder, entry.FullName);
                                if (!Directory.Exists(Path.GetDirectoryName(targetFile)))
                                    Directory.CreateDirectory(Path.GetDirectoryName(targetFile));
                                entry.CopyToFile(targetFile);

                                ((IProgress<int>) progress).Report(i);
                                i++;
                            }
                        }
                    }


                    var tConfigProgress = new Progress<int>();
                    var tConfig = new AsyncTask(tConfigProgress, 2, $"Installing configuration files");
                    tOverall.SubTasks.Add(tConfig);
                    if (!string.IsNullOrWhiteSpace(manifest.IconFile))
                    {
                        var entry = archive.GetEntry(manifest.IconFile);
                        entry.CopyToFile(Path.Combine(profile.ProfileFolder, manifest.IconFile));
                        profile.IconFile = manifest.IconFile;
                    }

                    archive.GetEntry(@"mods\mod-list.json")
                           ?.CopyToFile(Path.Combine(profile.ProfileFolder, @"mods\mod-list.json"));
                    ((IProgress<int>) tConfigProgress).Report(1);
                    archive.GetEntry(@"mods\mod-settings.json")
                           ?.CopyToFile(Path.Combine(profile.ProfileFolder, @"mods\mod-settings.json"));
                    ((IProgress<int>) tConfigProgress).Report(2);
                }
                profile.ReloadMods();
                profile.InProgress = false;

                return profile;
            });
        }

		public IEnumerator<Profile> GetEnumerator() => this.profiles.GetEnumerator();

		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

		public int IndexOf(Profile item) => this.profiles.IndexOf(item);

		public void Add(Profile item)
		{
			this.profiles.Add(item);
			this.localIdMap.Add(item.LocalId, this.profiles.Count - 1);
			item.PropertyChanged += this.ProfileOnPropertyChanged;
			this.Save();
		}

		public void Insert(int index, Profile item)
		{
			this.profiles.Insert(index, item);

			for (int i = index; i < this.profiles.Count; i++)
			{
				var p = this.profiles[i];
				this.localIdMap[p.LocalId] = i;
			}

			item.PropertyChanged += this.ProfileOnPropertyChanged;
			this.Save();
		}

		public bool Remove(Profile item)
		{
			var success = this.profiles.Remove(item);
			this.localIdMap.Remove(item.Id);
			if (success)
				item.PropertyChanged -= this.ProfileOnPropertyChanged;
			return success;
		}

		public void RemoveAt(int index)
		{
			var item = this.profiles[index];
			this.profiles.RemoveAt(index);
			this.localIdMap.Remove(item.LocalId);
			item.PropertyChanged -= this.ProfileOnPropertyChanged;
		}

		public void Clear()
		{
			foreach (var profile in profiles)
			{
				profile.PropertyChanged -= this.ProfileOnPropertyChanged;
			}
			this.localIdMap.Clear();
			this.profiles.Clear();
		}

		public bool Contains(Profile item) => this.profiles.Contains(item);

		public void CopyTo(Profile[] array, int arrayIndex) => this.profiles.CopyTo(array, arrayIndex);


		public event NotifyCollectionChangedEventHandler CollectionChanged
		{
			add => this.profiles.CollectionChanged += value;
			remove => this.profiles.CollectionChanged -= value;
		}

		public event PropertyChangedEventHandler PropertyChanged
		{
			add => (this.profiles as INotifyPropertyChanged).PropertyChanged += value;
			remove => (this.profiles as INotifyPropertyChanged).PropertyChanged -= value;
		}
	}
}