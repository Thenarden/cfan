﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CFAN.Downloads;
using CFAN.Factorio.Api;
using CFAN.Model;

namespace CFAN.Context
{
	public class CacheManager
	{
		public string GameVersionsDownloadFolder => Path.Combine(this.Settings.DownloadCacheFolder, "GameVersions");
		public string ModsDownloadFolder => Path.Combine(this.Settings.DownloadCacheFolder, "Mods");

	    public AppSettings Settings { get; }

        public CacheManager(AppSettings settings)
	    {
	        this.Settings = settings;
	    }


	    #region Mod Cache

        public string CacheModFile(ModInfoBase modInfo, Version version) =>
			Path.Combine(ModsDownloadFolder, modInfo.GetFilenameForVersion(version, archive: true));

		public bool CacheContainsMod(ModInfoBase modInfo, Version version)
		{
			return File.Exists(this.CacheModFile(modInfo, version));
		}

	    public LocalModFile GetCachedMod(ModInfoBase modInfo, Version version)
	    {
	        if (!this.CacheContainsMod(modInfo, version))
                throw new InvalidOperationException();

            return new LocalModFile(this.CacheModFile(modInfo, version), modInfo, version);
	    }

        #endregion


        #region GameVersion Cache
        

	    public string CacheGameVersionFile(GameVersion gameVersion, Platform platform) =>
	        Path.Combine(GameVersionsDownloadFolder, gameVersion.GetFilenameForPlatform(platform));

	    public bool CacheContainsGameVersion(GameVersion gameVersion, Platform platform)
	    {
	        return File.Exists(this.CacheGameVersionFile(gameVersion, platform));
	    }

	    public LocalGameVersionFile GetCachedGameVersion(GameVersion gameVersion, Platform platform)
	    {
	        if (!this.CacheContainsGameVersion(gameVersion, platform))
	            throw new InvalidOperationException();

	        return new LocalGameVersionFile(this.CacheGameVersionFile(gameVersion, platform), gameVersion, platform);
	    }


        #endregion


        #region ModInfoCache



        #endregion


        #region GameVersionCache

        

        #endregion

    }
}
