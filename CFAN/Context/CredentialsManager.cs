﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using CFAN.Model;
using Newtonsoft.Json;
using Shortbit.Utils.Collections.Generic;

namespace CFAN.Context
{
	public class CredentialsManager : IEnumerable<KeyValuePair<string, Credentials>>, INotifyCollectionChanged, INotifyPropertyChanged
	{
		private class Store
		{

			[JsonProperty(Required = Required.Always)]
			public string Key { get; set; }

			[JsonProperty(Required = Required.Always)]
			public string Username { get; set; }

			[JsonProperty(Required = Required.Default)]
			public string Entropy { get; set; }
		}

		public static string CredentialsFile => Path.Combine(AppSettings.AppDataFolder, "credentials.json");
		public static string CredentialsFolder => Path.Combine(AppSettings.AppDataFolder, "Credentials");


		public Credentials this[string key]
		{
			get
			{
				if (key == null)
					key = this.settings.DefaultCredentials;

				return this.credentials[key];
			}
		}

		public Credentials Default => this[null];

		public int Count => this.credentials.Count;

		public bool ContainsDefaultCredentials => this.ContainsKey(this.settings.DefaultCredentials);

		public ICollection<string> Keys => this.credentials.Keys;
		public ICollection<Credentials> All => this.credentials.Values;


		private readonly AppSettings settings;


		private readonly ObservableDictionary<string, Credentials> credentials;
		

		public CredentialsManager(AppSettings settings)
		{
			this.settings = settings;

			if (!Directory.Exists(CredentialsFolder))
				Directory.CreateDirectory(CredentialsFolder);

			

			var creds = new Dictionary<string, Credentials>();

			if (File.Exists(CredentialsFile))
			{
				var store = JsonConvert.DeserializeObject<List<Store>>(File.ReadAllText(CredentialsFile));
				foreach (var s in store)
				{
					var credFile = Path.Combine(CredentialsFolder, s.Key + ".cred");
					if (!File.Exists(credFile))
						continue;

					var entropy = Convert.FromBase64String(s.Entropy);
					var encryptedPassword = File.ReadAllBytes(credFile);

					var cred = new Credentials(s.Username, entropy, encryptedPassword);

					cred.PropertyChanged += CredentialOnPropertyChanged;

					creds.Add(s.Key, cred);
				}
			}
			this.credentials = new ObservableDictionary<string, Credentials>(creds);
		}

		private void Save(string key, Credentials credentials)
		{
			var s = new Store
			{
				Key = key,
				Username = credentials.Username,
				Entropy = Convert.ToBase64String(credentials.Entropy)
			};

			var credFile = Path.Combine(CredentialsFolder, s.Key + ".cred");
			File.WriteAllBytes(credFile, credentials.EncryptedPassword);

			var stores = this.credentials.Select(pair => new Store
			{
				Key = pair.Key,
				Username = pair.Value.Username,
				Entropy = Convert.ToBase64String(pair.Value.Entropy)
			}).ToList();

			File.WriteAllText(CredentialsFile, 
				JsonConvert.SerializeObject(stores, Formatting.Indented));
		}

		public void Add(string key, Credentials credentials)
		{
			if (this.All.Contains(credentials))
				throw new ArgumentException("CredentialsManager can contain each credential only once.");

			this.credentials.Add(key, credentials);
			credentials.PropertyChanged += this.CredentialOnPropertyChanged;
			Save(key, credentials);
		}

		public void Remove(string key)
		{
			var cred = this.credentials[key];
			cred.PropertyChanged -= this.CredentialOnPropertyChanged;
			this.credentials.Remove(key);
		}

		public bool ContainsKey(string key)
		{
			return this.credentials.ContainsKey(key);
		}

		
		

		private void CredentialOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
		{
			foreach (var pair in this.credentials)
			{
				if (pair.Value == sender)
				{
					this.Save(pair.Key, pair.Value);
					break;
				}
			}
		}

		public event NotifyCollectionChangedEventHandler CollectionChanged
		{
			add => this.credentials.CollectionChanged += value;
			remove => this.credentials.CollectionChanged -= value;
		}
		public event PropertyChangedEventHandler PropertyChanged
		{
			add => this.credentials.PropertyChanged += value;
			remove => this.credentials.PropertyChanged -= value;
		}

		public IEnumerator<KeyValuePair<string, Credentials>> GetEnumerator() => this.credentials.GetEnumerator();

		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
	}
}
