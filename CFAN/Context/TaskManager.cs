﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CFAN.Tasks;
using Newtonsoft.Json;
using Shortbit.Utils.Collections.ObjectModel;

namespace CFAN.Context
{
    public class TaskManager : IList<BackgroundTask>, INotifyCollectionChanged, INotifyPropertyChanged
    {

		public BackgroundTask this[int index]
		{
			get => this.tasks[index];
			set => this.tasks[index] = value;
		}
        

		public int Count => this.tasks.Count;
		public bool IsReadOnly => (this.tasks as ICollection<BackgroundTask>).IsReadOnly;
        

		private readonly AsyncObservableCollection<BackgroundTask> tasks;
        private readonly AppSettings settings;

		public TaskManager(AppSettings settings)
        {
            this.settings = settings;
			this.tasks = new AsyncObservableCollection<BackgroundTask>();
		}

		private void Task_OnPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            var task = (BackgroundTask) sender;
            if (args.PropertyName == nameof(BackgroundTask.State))
                this.Task_StateChanged(task);

		}

        private void Task_StateChanged(BackgroundTask task)
        {
            if (task.State == BackgroundTaskState.Failed)
            {
                Task.Run(() =>
                {
                    Thread.Sleep(5000);
                    this.Remove(task);
                });
            }
            else if (task.State == BackgroundTaskState.Aborted)
            {

                Task.Run(() =>
                {
                    Thread.Sleep(5000);
                    this.Remove(task);
                });
            }
            else if (task.State == BackgroundTaskState.Successful)
            {
                Task.Run(() =>
                {
                    Thread.Sleep(500);
                    this.Remove(task);
                });
            }
        }


        public IEnumerator<BackgroundTask> GetEnumerator() => this.tasks.GetEnumerator();

		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

		public int IndexOf(BackgroundTask item) => this.tasks.IndexOf(item);

		public void Add(BackgroundTask item)
		{
			this.tasks.Add(item);
			item.PropertyChanged += this.Task_OnPropertyChanged;
		}

		public void Insert(int index, BackgroundTask item)
		{
			this.tasks.Insert(index, item);

			for (int i = index; i < this.tasks.Count; i++)
			{
				var p = this.tasks[i];
			}

			item.PropertyChanged += this.Task_OnPropertyChanged;
		}

		public bool Remove(BackgroundTask item)
		{
			var success = this.tasks.Remove(item);
			if (success)
				item.PropertyChanged -= this.Task_OnPropertyChanged;
			return success;
		}

		public void RemoveAt(int index)
		{
			var item = this.tasks[index];
			this.tasks.RemoveAt(index);
			item.PropertyChanged -= this.Task_OnPropertyChanged;
		}

		public void Clear()
		{
			foreach (var profile in this.tasks)
			{
				profile.PropertyChanged -= this.Task_OnPropertyChanged;
			}
			this.tasks.Clear();
		}

		public bool Contains(BackgroundTask item) => this.tasks.Contains(item);

		public void CopyTo(BackgroundTask[] array, int arrayIndex) => this.tasks.CopyTo(array, arrayIndex);


		public event NotifyCollectionChangedEventHandler CollectionChanged
		{
			add => this.tasks.CollectionChanged += value;
			remove => this.tasks.CollectionChanged -= value;
		}

		public event PropertyChangedEventHandler PropertyChanged
		{
			add => (this.tasks as INotifyPropertyChanged).PropertyChanged += value;
			remove => (this.tasks as INotifyPropertyChanged).PropertyChanged -= value;
		}
    }
}
