﻿using System;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using Newtonsoft.Json;

namespace CFAN.Context
{
	public class AppSettings : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;


		public static string AppDataFolder
		{

			get
			{
				// TODO: Add paths for other platforms (linux etc)
				if (Environment.OSVersion.Platform == PlatformID.Win32NT)
					return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "CFAN");

				throw new NotImplementedException(
					$"AppData folder mapping is not implemented for Platform {Environment.OSVersion.ToString()}");

			}
		}

		public static string SettingsFilePath => Path.Combine(AppDataFolder, "settings.json");

		public AppSettings()
		{
			this.PropertyChanged += (sender, args) => this.MarkDirty();
		}

		[JsonIgnore]
		public bool HasChanged { get; private set; }

		public void MarkDirty()
		{
			this.HasChanged = true;
		}

		public string DefaultCredentials { get; set; } = "Factorio";

		public string DefaultProfileFolder { get; set; }

		public string LastExportFolder { get; set; }

		public string InstallationFolder { get; set; }

		public string DownloadCacheFolder { get; set; }

		public bool MinimizeToTray { get; set; } = true;


        public int SuccessfullTaskDestroyDelay { get; set; } = 2000;

        public int FailedTaskDestroyDelay { get; set; } = 10000;


        public static AppSettings LoadDefault()
		{
			string codeBase = Assembly.GetExecutingAssembly().CodeBase;
			UriBuilder uri = new UriBuilder(codeBase);
			string path = Uri.UnescapeDataString(uri.Path);
			var executingFolder =  Path.GetDirectoryName(path);

//#if DEBUG
			var instFolder = executingFolder;
//#else
//			var instFolder = Path.GetFullPath(Path.Combine(executingFolder, ".."));
//#endif

			return new AppSettings
			{
				InstallationFolder = instFolder,
				DefaultProfileFolder = Path.Combine(instFolder, "Profiles"),
				DownloadCacheFolder = Path.Combine(instFolder, "Downloads"),

				HasChanged = true
			};
		}

		public static AppSettings LoadFromFile(string file = null)
		{
			if (string.IsNullOrEmpty(file))
				file = SettingsFilePath;

			if (!File.Exists(file))
				return null;

			var settings = JsonConvert.DeserializeObject<AppSettings>(File.ReadAllText(file));
			if (string.IsNullOrWhiteSpace(settings.DefaultProfileFolder))
				settings.DefaultProfileFolder = Path.Combine(settings.InstallationFolder, "ProfileView");
			if (string.IsNullOrWhiteSpace(settings.DownloadCacheFolder))
				settings.DownloadCacheFolder = Path.Combine(settings.InstallationFolder, "Downloads");

			return settings;
		}

		public static AppSettings LoadFromFileOrDefault(string file = null)
		{
			var settings = AppSettings.LoadFromFile(file);
			if (settings == null)
			{
				settings = AppSettings.LoadDefault();
				settings.Save(file);
			}
			return settings;
		}

		public void Save(string file = null)
		{
			if (!this.HasChanged)
				return;
			
			if (string.IsNullOrEmpty(file))
				file = SettingsFilePath;

			var folder = Path.GetDirectoryName(file);
			if (!Directory.Exists(folder))
				Directory.CreateDirectory(folder);

			var content = JsonConvert.SerializeObject(this, Formatting.Indented);

			File.WriteAllText(file, content);
		}

		protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
		{
			PropertyChanged?.Invoke(this, e);
		}
	}
}
