﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CFAN.Factorio.Api;
using CFAN.Model;
using CFAN.Tasks;
using Shortbit.Utils;

namespace CFAN.Context
{
    public class ModManager
    {

        private AppContext context;

        
        public ModManager(AppContext context)
        {
            this.context = context;
        }

        public Task UninstallMod(Profile profile, ModInfoBase modInfo, CompositeTask containingTask = null)
        {
            return this.UninstallMod(profile, modInfo.Name, containingTask);
        }
        public Task UninstallMod(Profile profile, string modName, CompositeTask containingTask = null)
        {
            var tOverall = new AsyncTask($"Uninstalling mod {modName}");
            tOverall.AddTo(containingTask, this.context.Tasks);

            var task = Task.Run(() =>
            {
                if (!profile.Mods.Contains(modName))
                    return;
                var mod = profile.Mods[modName];
                if (!PathEx.Exists(mod.Path))
                    return;

                if (mod.IsArchive)
                    File.Delete(mod.Path);
                else
                    Directory.Delete(mod.Path, true);

                profile.ReloadMods();
            });
            tOverall.Task = task;
            return task;
        }

        public async Task InstallMod(Profile profile, ModInfoBase modInfo, Version version, bool uninstallExisting = true, CompositeTask containingTask = null)
        {
            var tOverall = new CompositeTask(uninstallExisting && profile.Mods.Contains(modInfo.Name) ? 3 : 2, $"Installing mod {modInfo.Title}");
            tOverall.AddTo(containingTask, this.context.Tasks);

            if (profile.Mods.Contains(modInfo.Name))
            {
                if (uninstallExisting)
                    await this.UninstallMod(profile, modInfo, tOverall);
                else
                    throw new InvalidOperationException("Can't install already existing mod.");
            }

            var modFile = this.context.Downloads.DownloadMod(modInfo, version);
            var tDownload = new DownloadTask(modFile);
            tOverall.SubTasks.Add(tDownload);

            await modFile.InstallTo(profile.ModsFolder, tOverall);
            profile.ReloadMods();
        }
    }
}
