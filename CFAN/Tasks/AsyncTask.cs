﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CFAN.Downloads;
using CFAN.Tasks.Waiting;
using Shortbit.Utils;

namespace CFAN.Tasks
{
    public class AsyncTask : BackgroundTask
    {
        private Task task;
        public Task Task
        {
            get => this.task;
            set
            {
                if (this.task != null)
                {
                    this.stateUpdaterCancel.Cancel();
                    this.stateUpdateTask.Wait();
                }

                this.task = value;
                if (this.task != null)
                {
                    this.waitable = new TaskWaitable(this.Task);
                    this.stateUpdaterCancel = new CancellationTokenSource();
                    this.stateUpdateTask =
                        this.task.ContinueWith(continuationAction:this.UpdateState, 
                                               cancellationToken:this.stateUpdaterCancel.Token);
                    this.UpdateState(null);
                }
                else
                {
                    this.State = BackgroundTaskState.Waiting;
                }
            }

        }

        private Task stateUpdateTask;
        private CancellationTokenSource stateUpdaterCancel;
        private IWaitable waitable;
        public override IWaitable Waitable => this.waitable;

        private Progress<int> progressReporter;
        private int reportedProgress;
        private int maxProgress;
        private readonly ManualResetEvent progressCompleteEvent = new ManualResetEvent(false);

        private bool markedFailed;
        private bool markedAborted;
        private bool markedSuccessfull;

        public AsyncTask()
            : this(null, null, 1, "")
        {}
        public AsyncTask(string description)
            : this(null, null, 1, description)
        {}
        public AsyncTask(Task task)
            : this(task, null, 1, "")
        {}
        public AsyncTask(Task task, string description)
            : this(task, null, 1, description)
        {}
        public AsyncTask(Progress<int> progess, int maxProgress)
            : this(null, progess, maxProgress, "")
        {}
        public AsyncTask(Progress<int> progess, int maxProgress, string description)
            : this(null, progess, maxProgress, description)
        {}
        public AsyncTask(Task task, Progress<int> progess, int maxProgress)
        : this(task, progess, maxProgress, "")
        {}
        public AsyncTask(Task task, Progress<int> progess, int maxProgress, string description)
        {
            this.Task = task;

            this.reportedProgress = 0;
            this.progressReporter = progess;
            if (this.progressReporter != null)
                this.progressReporter.ProgressChanged += ProgressReporter_OnProgressChanged;
            this.maxProgress = maxProgress;

            this.NumSteps = maxProgress;
            this.Description = description;
        }

        private void ProgressReporter_OnProgressChanged(object sender, int i)
        {
            this.reportedProgress = i;
            this.UpdateState(null);
        }

        public void SetProgress(Progress<int> progess, int maxProgress)
        {
            if (this.progressReporter != null)
                this.progressReporter.ProgressChanged -= this.ProgressReporter_OnProgressChanged;
            this.progressReporter = progess;
            if (this.progressReporter != null)
                this.progressReporter.ProgressChanged += this.ProgressReporter_OnProgressChanged;
            this.reportedProgress = 0;
            this.maxProgress = maxProgress;
            this.UpdateState(null);
        }

        public void ManualMarkFailed()
        {
            this.markedFailed = true;
            this.UpdateState(null);
        }
        public void ManualMarkAborted()
        {
            this.markedAborted = true;
            this.UpdateState(null);
        }
        public void ManualMarkSuccessfull()
        {
            this.markedSuccessfull = true;
            this.UpdateState(null);
        }

        private void UpdateState(Task argT)
        {
            var task = this.Task;
            
            if (this.progressReporter != null)
            {
                this.CompletedSteps = MathEx.Clamp(this.reportedProgress, 0, this.maxProgress);
                this.NumSteps = this.maxProgress;
            }
            else if (task != null)
            {
                this.NumSteps = 1;
                this.CompletedSteps = task.IsCompleted ? 1 : 0;
            }
            else
            {
                this.NumSteps = 0;
                this.CompletedSteps = 0;
            }

            if (this.markedFailed)
                this.State = BackgroundTaskState.Failed;
            else if (this.markedAborted)
                this.State = BackgroundTaskState.Aborted;
            else if(this.markedSuccessfull)
            this.State = BackgroundTaskState.Successful;
            else if (task == null)
            {
                if (this.CompletedSteps <= 0)
                    this.State = BackgroundTaskState.Waiting;
                else if (this.CompletedSteps == this.NumSteps)
                    this.State = BackgroundTaskState.Successful;
                else
                    this.State = BackgroundTaskState.Running;
            }
            else
            {
                switch (task.Status)
                {
                    case TaskStatus.Created:
                    case TaskStatus.WaitingForActivation:
                    case TaskStatus.WaitingToRun:
                        this.State = BackgroundTaskState.Waiting;
                        break;
                    case TaskStatus.Running:
                    case TaskStatus.WaitingForChildrenToComplete:
                        this.State = BackgroundTaskState.Running;
                        break;
                    case TaskStatus.Canceled:
                        this.State = BackgroundTaskState.Aborted;
                        break;
                    case TaskStatus.Faulted:
                        this.State = BackgroundTaskState.Failed;
                        break;
                    case TaskStatus.RanToCompletion:
                        this.State = BackgroundTaskState.Successful;
                        this.CompletedSteps = this.NumSteps;
                        break;
                }
            }




        }
    }
}
