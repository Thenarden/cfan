﻿using System;
using System.Threading;

namespace CFAN.Tasks.Waiting
{
    public class WrappedWaitable : IWaitable
    {
        public WaitHandle WaitHandle { get; }

        public WrappedWaitable(WaitHandle wrapped)
        {
            this.WaitHandle = wrapped;
                
        }

        public bool WaitOne(int millisecondsTimeout)
        {
            return this.WaitHandle.WaitOne(millisecondsTimeout);
        }
    }
}
