﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CFAN.Tasks.Waiting
{
    public class TaskWaitable : IWaitable
    {
        public Task Task { get; }
        public CancellationToken? CancellationToken { get; }

        public TaskWaitable(Task task, CancellationToken? cancellationToken = null)
        {
            this.Task = task;
            this.CancellationToken = cancellationToken;
        }
        
        public bool WaitOne(int millisecondsTimeout)
        {
            if (this.CancellationToken.HasValue)
                this.Task.Wait(millisecondsTimeout, this.CancellationToken.Value);
            else
                this.Task.Wait(millisecondsTimeout);

            return this.Task.IsCompleted;
        }
    }
}
