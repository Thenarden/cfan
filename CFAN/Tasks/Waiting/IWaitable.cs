﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CFAN.Tasks.Waiting
{
    public interface IWaitable
    {
        bool WaitOne(int millisecondsTimeout);
    }

    public static class Waitable
    {
        public static IWaitable Create(WaitHandle handle)
        {
            return new WrappedWaitable(handle);
        }
        public static IWaitable Create(Task task)
        {
            return new TaskWaitable(task);
        }
        public static IWaitable Create(Task task, CancellationToken token)
        {
            return new TaskWaitable(task, token);
        }


        public static bool WaitOne(this IWaitable me)
        {
            return me.WaitOne(-1);
        }
        public static bool WaitOne(this IWaitable me, TimeSpan timeout)
        {
            return me.WaitOne((int)timeout.TotalMilliseconds);
        }

        public static Task Await(this IWaitable me)
        {
            return me.Await(-1);
        }
        public static Task Await(this IWaitable me, int millisecondsTimeout)
        {
            return Task.Run(() => me.WaitOne(millisecondsTimeout));
        }
        public static Task Await(this IWaitable me, TimeSpan timeout)
        {
            return me.Await((int)timeout.TotalMilliseconds);
        }
    }
}
