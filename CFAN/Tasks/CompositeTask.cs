﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CFAN.Tasks.Waiting;
using PropertyChanged;
using Shortbit.Utils;
using Shortbit.Utils.Collections.ObjectModel;

namespace CFAN.Tasks
{
    public class CompositeTask : BackgroundTask
    {
        public AsyncObservableCollection<SubTask> SubTasks { get; }

        public override IWaitable Waitable { get; }
        

        private readonly ManualResetEvent completedEvent;
        private int upcommingSteps;
        private bool criticalTaskFailed;
        private bool criticalTaskAborted;

        public CompositeTask()
            : this(null, 0, "")
        {}
        public CompositeTask(IEnumerable<SubTask> subTasks)
            : this(subTasks, 0, "")
        {}
        public CompositeTask(string description)
            : this(null, 0, description)
        {}
        public CompositeTask(IEnumerable<SubTask> subTasks, string description)
            : this(subTasks, 0, description)
        {}
        public CompositeTask(int upcommingSteps)
            : this(null, upcommingSteps, "")
        {}
        public CompositeTask(IEnumerable<SubTask> subTasks, int upcommingSteps)
            : this(subTasks, upcommingSteps, "")
        {}
        public CompositeTask(int upcommingSteps, string description)
        : this(null, upcommingSteps, description)
        {}
        public CompositeTask(IEnumerable<SubTask> subTasks, int upcommingSteps, string description)
        {
            if (subTasks == null)
                subTasks = new SubTask[0];

            this.Description = description ?? "";
            this.upcommingSteps = upcommingSteps;

            this.completedEvent = new ManualResetEvent(false);
            this.Waitable = new WrappedWaitable(this.completedEvent);

            this.SubTasks = new AsyncObservableCollection<SubTask>(subTasks);

            this.SubTasks.CollectionChanged += this.SubTasks_OnCollectionChanged;
            this.NumSteps = SubTasks.Count + upcommingSteps;
            this.CompletedSteps = 0;

            foreach (var subTask in this.SubTasks)
            {
                subTask.Task.PropertyChanged += SubTask_OnPropertyChanged;
            }
        }

        private void SubTasks_OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs args)
        {
            if (args.Action == NotifyCollectionChangedAction.Remove 
                || args.Action == NotifyCollectionChangedAction.Replace)
            {
                foreach (SubTask subTask in args.OldItems)
                {
                    subTask.Task.PropertyChanged -= SubTask_OnPropertyChanged;
                }
            }
            if (args.Action == NotifyCollectionChangedAction.Add || args.Action == NotifyCollectionChangedAction.Replace)
            {
                foreach (SubTask subTask in args.NewItems)
                {
                    subTask.Task.PropertyChanged += SubTask_OnPropertyChanged;
                    this.UpdateState(subTask);
                }
            }
        }

        private void SubTask_OnPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            var task = (BackgroundTask)sender;
            var subTask = this.SubTasks.FirstOrDefault(st => st.Task == task);
            if (subTask == null)
            {
                task.PropertyChanged -= this.SubTask_OnPropertyChanged;
                return;
            }

            if (args.PropertyName == nameof(BackgroundTask.State))
            {
                this.UpdateState(subTask);
            }
        }

        private void UpdateState(SubTask subTask)
        {
            if (subTask.Task.State == BackgroundTaskState.Failed)
            {
                this.CompletedSteps++;
                this.criticalTaskFailed |= subTask.FailesParentTask;
                Task.Run(() =>
                {
                    Thread.Sleep(5000);
                    this.SubTasks.Remove(subTask);
                });
            }
            else if (subTask.Task.State == BackgroundTaskState.Aborted)
            {

                this.CompletedSteps++;
                this.criticalTaskAborted |= subTask.AbortsParentTask;
                Task.Run(() =>
                {
                    Thread.Sleep(5000);
                    this.SubTasks.Remove(subTask);
                });
            }
            else if (subTask.Task.State == BackgroundTaskState.Successful)
            {
                this.CompletedSteps++;
                Task.Run(() =>
                {
                    Thread.Sleep(500);
                    this.SubTasks.Remove(subTask);
                });
            }
            else if (subTask.Task.State == BackgroundTaskState.Running)
                this.State = BackgroundTaskState.Running;


            if (this.criticalTaskFailed && this.State != BackgroundTaskState.Failed)
            {
                this.completedEvent.Set();
                this.State = BackgroundTaskState.Failed;
            }
            else if (this.criticalTaskAborted && this.State != BackgroundTaskState.Aborted && this.State != BackgroundTaskState.Failed)
            {
                this.completedEvent.Set();
                this.State = BackgroundTaskState.Aborted;
            }
            else if (this.CompletedSteps >= this.NumSteps)
            {
                this.completedEvent.Set();
                this.State = BackgroundTaskState.Successful;
            }
            else if (this.completedEvent.WaitOne(0))
                this.completedEvent.Reset();
        }
        

        public SubTask GetRunningSubTask()
        {
            return this.SubTasks.FirstOrDefault(t => t.Task.State == BackgroundTaskState.Running);
        }
        public IEnumerable<SubTask> GetRunningSubTasks()
        {
            return this.SubTasks.Where(t => t.Task.State == BackgroundTaskState.Running);
        }

        
        public class SubTask
        {
            public SubTask(BackgroundTask task)
            {
                this.Task = task;
            }

            public BackgroundTask Task { get; }

            public bool FailesParentTask { get; set; } = true;

            public bool AbortsParentTask { get; set; } = true;

            public static implicit operator SubTask (BackgroundTask task)
            {
                return new SubTask(task){FailesParentTask = true};
            }
        }
        
        
    }

}
