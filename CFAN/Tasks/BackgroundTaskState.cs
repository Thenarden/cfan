﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CFAN.Tasks
{
    public enum BackgroundTaskState
    {
        Waiting,
        Running,
        Successful,
        Aborted,
        Failed
    }
}
