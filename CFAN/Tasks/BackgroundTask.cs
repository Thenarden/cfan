﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CFAN.Context;
using CFAN.Tasks.Waiting;

namespace CFAN.Tasks
{
    public abstract class BackgroundTask : INotifyPropertyChanged
    {
        public double Progress => this.NumSteps > 0 ? this.CompletedSteps / (double)this.NumSteps : 0;

        public int CompletedSteps { get; protected set; }

        public int NumSteps { get; protected set; }

        public abstract IWaitable Waitable { get; }

        public string Description { get; protected set; }

        public BackgroundTaskState State { get; protected set; } = BackgroundTaskState.Waiting;


        public void AddTo(CompositeTask containingTask, TaskManager manager)
        {
            if (containingTask != null)
                containingTask.SubTasks.Add(this);
            else
                manager.Add(this);
        }
        

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            this.PropertyChanged?.Invoke(this, e);
            if (e.PropertyName == "Description")
                Console.WriteLine();
        }


        public static bool IsCompletedState(BackgroundTaskState state)
        {
            return state == BackgroundTaskState.Aborted || state == BackgroundTaskState.Successful ||
                   state == BackgroundTaskState.Failed;
        }

    }
}
