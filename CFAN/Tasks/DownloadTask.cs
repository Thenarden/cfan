﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CFAN.Downloads;
using CFAN.Tasks.Waiting;
using CFAN.Utils;

namespace CFAN.Tasks
{
    public class DownloadTask : BackgroundTask
    {
        public override IWaitable Waitable => this.waitable;
        private IWaitable waitable;

        private BackgroundDownload download;

        public BackgroundDownload Download
        {
            get => this.download;
            set
            {
                if (this.download != null)
                {
                    this.download.PropertyChanged -= DownloadOnPropertyChanged;
                    this.download.Progress.ProgressUpdate -= ProgressOnProgressUpdate;
                }
                this.download = value;
                if (this.download == null)
                {
                    this.waitable = new WrappedWaitable(new ManualResetEvent(false));
                    this.Description = "No download connected";
                    this.CompletedSteps = 0;
                    this.NumSteps = 0;
                    this.State = BackgroundTaskState.Waiting;
                }
                else
                {
                    this.waitable = new WrappedWaitable(this.download.WaitHandle);
                    this.download.PropertyChanged += DownloadOnPropertyChanged;
                    this.download.Progress.ProgressUpdate += ProgressOnProgressUpdate;
                    this.UpdateProgress();
                }
                
            }
        }

        public DownloadTask()
        : this((LocalFile)null)
        {}

        public DownloadTask(BackgroundDownload download)
        {
            this.Download = download;
        }
        public DownloadTask(LocalFile file)
        {
            this.Download = file.RunningDownload;
            if (file.IsFinished)
            {
                this.Description = "Download already finished";
                this.CompletedSteps = 1;
                this.NumSteps = 1;
                this.State = BackgroundTaskState.Successful;
            }
        }

        private void UpdateProgress()
        {
            if (this.download.State == BackgroundDownloadState.Aborted)
                this.State = BackgroundTaskState.Aborted;
            else if (this.download.State == BackgroundDownloadState.Completed)
            {
                this.State = BackgroundTaskState.Successful;
                this.Description = "Completed";
                this.CompletedSteps = 100;
                this.NumSteps = 100;
            }
            else if (this.download.State == BackgroundDownloadState.Waiting)
            {
                this.State = BackgroundTaskState.Waiting;
                this.Description = "Waiting in queue";
                this.CompletedSteps = 0;
                this.NumSteps = 0;
            }
            else if (this.download.State == BackgroundDownloadState.Connecting)
            {
                this.State = BackgroundTaskState.Running;
                this.Description = "Connecting";
                this.CompletedSteps = 0;
                this.NumSteps = 0;
            }
            else if (this.download.State == BackgroundDownloadState.Downloading)
            {
                this.State = BackgroundTaskState.Running;
                
                this.CompletedSteps = (int) (this.download.Progress.Relative * 100);
                this.NumSteps = 100;

                this.Description = $"Downloading - {this.CompletedSteps}% ({this.download.Progress.Speed.ToHumanReadableSize()}/s, {this.download.Progress.Remaining.ToHumanReadableSize()} remaining)";
            }
        }
        private void ProgressOnProgressUpdate(object sender, EventArgs eventArgs)
        {
            this.UpdateProgress();
        }

        private void DownloadOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (propertyChangedEventArgs.PropertyName == nameof(BackgroundDownload.State))
            {
                this.UpdateProgress();
            }
        }
    }
}
