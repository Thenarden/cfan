﻿using System.Collections.Generic;
using CFAN.Downloads;
using CFAN.Factorio.Api;
using CFAN.Model;
using CFAN.Utils;
using Newtonsoft.Json.Linq;

namespace CFAN.Factorio.Adapter
{
	public interface IGameVersionAdapter
	{
		AppContext Context { get; }

		// For some bizzare reasong (TypeLoadException with non-virtual method), these two can't be properties
		string GetName();
		string GetTitle();

		void LoadOptions(JObject options);

		IEnumerable<GameVersion> ListGameVersions();



		BackgroundDownload DownloadGameVersion(GameVersion version, Platform platform);
	}
}
