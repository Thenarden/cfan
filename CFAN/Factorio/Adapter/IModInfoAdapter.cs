﻿using System;
using System.Collections.Generic;
using CFAN.Downloads;
using CFAN.Factorio.Api;
using CFAN.Utils;
using Newtonsoft.Json.Linq;

namespace CFAN.Factorio.Adapter
{
	public interface IModInfoAdapter
	{
		AppContext Context { get; }

		// For some bizzare reasong (TypeLoadException with non-virtual method), these two can't be properties
		string GetName();
		string GetTitle();

		void LoadOptions(JObject options);


		ModInfoList GetModList(string query = null, string owner = null, string tags = null, int page = 1, int pageSize = -1,
			ModListOrder order = ModListOrder.RecentlyUpdated);

		ModInfoFull GetModInfo(string name);

		IEnumerable<ModInfoFull> GetExtendedModList(string query = null, string owner = null, string tags = null,
													 Version factorioVersion = null,
													 ModListOrder order = ModListOrder.RecentlyUpdated);


		BackgroundDownload DownloadMod(ModInfoBase modInfo, Version releaseVersion);
	}
}
