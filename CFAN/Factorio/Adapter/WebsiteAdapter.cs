﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using CFAN.Downloads;
using CFAN.Exceptions;
using CFAN.Factorio.Api;
using CFAN.Model;
using CFAN.Utils;
using HtmlAgilityPack;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using Shortbit.Utils;
using Shortbit.Utils.Collections.Generic;

namespace CFAN.Factorio.Adapter
{
	public class WebsiteAdapter : IGameVersionAdapter, IModInfoAdapter
	{

		private static readonly Regex VersionUrlRegex = new Regex(@"/get-download/(?<version>\d+\.\d+(\.\d+))/(?<build>\w+)/(?<distro>[\w_-]+)");

		private string credentialsKey;

		private readonly CookieContainer gameVersionCookies = new CookieContainer();
		private readonly CookieContainer modCookies = new CookieContainer();

		private string apiUsername = null;
		private string apiToken = null;

		private readonly JsonSerializerSettings modApiSettings;

		public AppContext Context { get; }

		public string GetName() => "WebsiteAdapter";

		public string GetTitle() => "Website Http Mod & Gameversion Adapter";


		public WebsiteAdapter(AppContext context)
		{
			this.Context = context;

			this.modApiSettings = new JsonSerializerSettings
			{
				Converters = new List<JsonConverter> {
					new FactoryConverter<ModInfoFull>(a => new ModInfoFull(
						this, 
						a.Deserialize<int>("id"),
						a.Deserialize<string>("name"))),

					new FactoryConverter<ModInfoShort>(a => new ModInfoShort(
						this,
						a.Deserialize<int>("id"),
						a.Deserialize<string>("name"))),
				},
				DateFormatHandling = DateFormatHandling.IsoDateFormat,
				ContractResolver = new DefaultContractResolver
				{
					NamingStrategy = new SnakeCaseNamingStrategy()
				}
			};
		}



		public void LoadOptions(JObject options)
		{
			this.credentialsKey = options["credentialsKey"]?.ToObject<string>();
		}


		#region IModInfoAdapter

		public ModInfoList GetModList(string query = null, string owner = null, string tags = null, int page = 1, int pageSize = -1,
			ModListOrder order = ModListOrder.RecentlyUpdated)
		{

			var args = new NameValueCollection();
			if (!string.IsNullOrWhiteSpace(query))
				args["q"] = query;
			if (!string.IsNullOrWhiteSpace(owner))
				args["owner"] = owner;
			if (!string.IsNullOrWhiteSpace(tags))
				args["tags"] = tags;
			if (page > 0)
				args["page"] = page.ToString();
			if (pageSize > 0)
				args["page_size"] = pageSize.ToString();
			//if (order != ModListOrder.RecentlyUpdated)
				args["order"] = ModListOrderInfo.Query[order];

			var builder = new UriBuilder("https://mods.factorio.com/api/mods/");
			builder.Query = args.UrlEncode();


			var request = (HttpWebRequest)WebRequest.Create(builder.Uri);
            
		    try
		    {
		        var response = (HttpWebResponse) request.GetResponse();
            
		        string content;
		        using (var stream = response.GetResponseStream())
		        using (var reader = new StreamReader(stream))
		        {
		            content = reader.ReadToEnd();
		        }

		        return JsonConvert.DeserializeObject<ModInfoList>(content, this.modApiSettings);
		    }
		    catch (WebException e)
		    {
		        if (((HttpWebResponse) e.Response).StatusCode == HttpStatusCode.NotFound)
		            return null;

		        throw;
		    }

		}

		public ModInfoFull GetModInfo(string name)
		{
			var request = (HttpWebRequest)WebRequest.Create($"https://mods.factorio.com/api/mods/{name}");

			var response = (HttpWebResponse) request.GetResponse();

			string content;
			using (var stream = response.GetResponseStream())
			using (var reader = new StreamReader(stream))
			{
				content = reader.ReadToEnd();
			}

			return JsonConvert.DeserializeObject<ModInfoFull>(content, this.modApiSettings);
		}

		public IEnumerable<ModInfoFull> GetExtendedModList(string query = null, string owner = null, string tags = null,
											  Version factorioVersion = null, ModListOrder order = ModListOrder.RecentlyUpdated)
		{
			return new ModListEnumerable(this, query, owner, tags, factorioVersion, order);
		}


		private Uri ReleaseDownloadUrl(Release release)
		{
			var builder = new UriBuilder(release.FullDownloadUrl);
			var args = new NameValueCollection();
			if (!string.IsNullOrEmpty(this.apiUsername))
				args["username"] = this.apiUsername;
			if (!string.IsNullOrEmpty(this.apiToken))
				args["token"] = this.apiToken;
			builder.Query = args.UrlEncode();
			return builder.Uri;
		}

		public BackgroundDownload DownloadMod(ModInfoBase modInfo, Version releaseVersion)
		{
			var mod = modInfo.GetFullInfo();

			var release = mod.Releases.FirstOrDefault(r => r.Version == releaseVersion);
			if (release == null)
				throw new ArgumentException($"Mod has no release with version {releaseVersion}");
			
			if (release.FullDownloadUrl == null)
				throw new ArgumentException($"Mod Release {releaseVersion} is missing its download url.");


			var request = (HttpWebRequest)WebRequest.Create(ReleaseDownloadUrl(release));
			request.Method = "GET";
			request.AllowAutoRedirect = false;
			request.CookieContainer = modCookies;

			var download = new BackgroundDownload(request)
			{
				BlockSize = 100000,
				CloseStreamOnFinish = true,

				RepeatRequestCallback = (oldReques, response) =>
				{
					if (this.ModRequiredAuthentication(response))
					{
						var newRequest = (HttpWebRequest)WebRequest.Create(ReleaseDownloadUrl(release));
						request.Method = "GET";
						request.AllowAutoRedirect = false;
						request.CookieContainer = modCookies;
						return newRequest;
					}
					if (response.StatusCode == HttpStatusCode.Found)
					{
						var h = response.Headers.ToDictionary();
						var newRequest = (HttpWebRequest)WebRequest.Create(response.Headers["Location"]);
						request.Method = "GET";
						request.AllowAutoRedirect = false;
						request.CookieContainer = modCookies;
						return newRequest;
					}
					return null;
				}
			};
			return download;
		}

		private bool ModRequiredAuthentication(HttpWebResponse response)
		{
			if (response.StatusCode != HttpStatusCode.Found)
				return false;

			if (response.Headers["Location"] == "https://mods.factorio.com/login")
			{
				response.Close();
				this.AuthenticateModPortal();
				return true;
			}
			return false;
		}

		private void AuthenticateModPortal()
		{
			Console.WriteLine("Requesting login form");
			var credentials = this.Context.Credentials[this.credentialsKey];  // Item accessor returns Default if key is null.


			var args = new NameValueCollection();
			args["api_version"] = "2";
			args["require_game_ownership"] = "true";

			args["username"] = credentials.Username;
			using (var pw = credentials.Password.ToCharArray())
				args["password"] = new string(pw.BackendArray);

			var formData = Encoding.ASCII.GetBytes(args.UrlEncode());

			Console.WriteLine("Posting login data");
			var loginRequest = (HttpWebRequest)WebRequest.Create("https://auth.factorio.com/api-login");
			loginRequest.Method = "POST";
			loginRequest.CookieContainer = modCookies;
			loginRequest.AllowAutoRedirect = false;
			loginRequest.ContentType = "application/x-www-form-urlencoded";

			loginRequest.ContentLength = formData.Length;
			using (var stream = loginRequest.GetRequestStream())
			{
				stream.Write(formData, 0, formData.Length);
			}

			Console.WriteLine("Awaiting response");
			string content;
			using (var response = (HttpWebResponse)loginRequest.GetResponse())
			{
				content = response.GetResponseContent();
				if (response.StatusCode != HttpStatusCode.OK)
					throw new AuthentificationFailedException();

			}

			dynamic auth = JObject.Parse(content);
			this.apiUsername = auth.username;
			this.apiToken = auth.token;


		}


		

		#endregion

		#region IGameVersionAdapter

        private List<GameVersion> GetGameVersions(string url, bool experimental)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.AllowAutoRedirect = false;
            request.CookieContainer = gameVersionCookies;

            var doc = new HtmlDocument();

            using (var response = (HttpWebResponse)request.GetResponse())
            {
                if (this.GameVersionRequiredAuthentication(response))
                    return GetGameVersions(url, experimental);

                using (var stream = response.GetResponseStream())
                {
                    doc.Load(stream);
                }
            }

            var nodes = doc.DocumentNode.SelectNodes("//body/div/ul//a");
            var releaseLinks = nodes.Select(n => n.GetAttributeValue("href", null)).ToArray();
            
            var versions = new SortedDictionary<Version, Dictionary<string, Dictionary<string, Uri>>>(new ReverseComparer<Version>());
            foreach (var link in releaseLinks)
            {
                var match = VersionUrlRegex.Match(link);
                if (!match.Success)
                    continue;

                var version = Version.Parse(match.Groups["version"].Value);
                var build = match.Groups["build"].Value;
                var distro = match.Groups["distro"].Value;

                if (!versions.ContainsKey(version))
                    versions.Add(version, new Dictionary<string, Dictionary<string, Uri>>());

                if (!versions[version].ContainsKey(build))
                    versions[version].Add(build, new Dictionary<string, Uri>());

                versions[version][build][distro] = new Uri("https://www.factorio.com" + link);
            }
            var res = new List<GameVersion>();
            foreach (var pair in versions)
            {
                var v = new GameVersion(pair.Key, this);
                v.Experimental = experimental;
                bool any = false;
                if (pair.Value.ContainsKey("alpha"))
                {
                    any = true;
                    v.Downloads.Win64 = pair.Value["alpha"].GetOrDefault("win64-manual", (Uri)null);
                    v.Downloads.Win32 = pair.Value["alpha"].GetOrDefault("win32-manual", (Uri)null);
                    v.Downloads.Osx = pair.Value["alpha"].GetOrDefault("osx", (Uri)null);
                    v.Downloads.Linux64 = pair.Value["alpha"].GetOrDefault("linux64", (Uri)null);
                }
                if (pair.Value.ContainsKey("headless"))
                {
                    any = true;
                    v.Downloads.HeadlessLinux64 = pair.Value["headless"].GetOrDefault("linux64", (Uri)null);
                }
                if (any)
                    res.Add(v);
            }
            return res;
        }
		public IEnumerable<GameVersion> ListGameVersions()
		{
            var release = GetGameVersions("https://www.factorio.com/download", false);
            var experimental = GetGameVersions("https://www.factorio.com/download/experimental", true);
            release.AddRange(experimental);
			return release;

		}

		public BackgroundDownload DownloadGameVersion(GameVersion version, Platform platform)
		{
			var url = version.Downloads.Platforms.GetOrDefault(platform);
			if (url == null)
				throw new ArgumentException($"GameVersion does not have a download for platform {platform}");


			var request = (HttpWebRequest)WebRequest.Create(url);
			request.Method = "GET";
			request.AllowAutoRedirect = false;
			request.CookieContainer = gameVersionCookies;

			var download = new BackgroundDownload(request)
			{
				BlockSize = 100000,
				CloseStreamOnFinish = true,

				RepeatRequestCallback = (oldReques, response) =>
				{
					if (this.GameVersionRequiredAuthentication(response))
					{
						var newRequest = (HttpWebRequest) WebRequest.Create(url);
						request.Method = "GET";
						request.AllowAutoRedirect = false;
						request.CookieContainer = gameVersionCookies;
						return newRequest;
					}
					if (response.StatusCode == HttpStatusCode.Found)
					{
						var newRequest = (HttpWebRequest)WebRequest.Create(response.Headers["Location"]);
						request.Method = "GET";
						request.AllowAutoRedirect = false;
						request.CookieContainer = gameVersionCookies;
						return newRequest;
					}
					return null;
				}
			};
			return download;
		}

		private bool GameVersionRequiredAuthentication(HttpWebResponse response)
		{
			if (response.StatusCode != HttpStatusCode.Found)
				return false;

			if (response.Headers["Location"] == "https://www.factorio.com/login")
			{
				response.Close();
				this.AuthenticateGameVersion();
				return true;
			}
			return false;
		}

		private void AuthenticateGameVersion()
		{
			Console.WriteLine("Requesting login form");
			var credentials = this.Context.Credentials[this.credentialsKey];  // Item accessor returns Default if key is null.


			var request = (HttpWebRequest)WebRequest.Create("https://www.factorio.com/login");
			request.Method = "GET";
			request.AllowAutoRedirect = false;
			request.CookieContainer = gameVersionCookies;


			var doc = new HtmlDocument();
			using (var response = (HttpWebResponse)request.GetResponse())
			{
				if (response.StatusCode != HttpStatusCode.OK)
					throw new AuthentificationFailedException();

				using (var stream = response.GetResponseStream())
				{
					doc.Load(stream);
				}
			}

			var CSRFtoken = doc.DocumentNode.SelectSingleNode("//input[@name=\"csrf_token\"]").GetAttributeValue("value", null);

			Console.WriteLine("Posting login data");
			var args = new NameValueCollection();
			args["csrf_token"] = CSRFtoken;
			args["username_or_email"] = credentials.Username;
			args["action"] = "Login";
			using (var pw = credentials.Password.ToCharArray())
				args["password"] = new string(pw.BackendArray);

			var formContent = args.UrlEncode();
			var formContentD = args.ToDictionary();
			var formData = Encoding.ASCII.GetBytes(args.UrlEncode());

			var loginRequest = (HttpWebRequest)WebRequest.Create("https://www.factorio.com/login");
			loginRequest.Method = "POST";
			loginRequest.CookieContainer = gameVersionCookies;
			loginRequest.AllowAutoRedirect = false;
			loginRequest.ContentType = "application/x-www-form-urlencoded";

			loginRequest.ContentLength = formData.Length;
			using (var stream = loginRequest.GetRequestStream())
			{
				stream.Write(formData, 0, formData.Length);
			}

			using (var loginResponse = (HttpWebResponse)loginRequest.GetResponse())
			{
				if (loginResponse.StatusCode != HttpStatusCode.Found)
					throw new AuthentificationFailedException();
			}

		}

		#endregion


	}
}
