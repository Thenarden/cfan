﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using CFAN.Factorio.Api;

namespace CFAN.Factorio.Adapter
{
	public class ModListEnumerable : IEnumerable<ModInfoFull>
	{
		public readonly IModInfoAdapter Adapter;

		public readonly string Query;
		public readonly string Owner;
		public readonly string Tags;
		public readonly Version FactorioVersion;
		public readonly ModListOrder Order;

		public ModListEnumerable(IModInfoAdapter adapter, string query = null, string owner = null, string tags = null,
								 Version factorioVersion = null, ModListOrder order = ModListOrder.RecentlyUpdated)
		{
			this.Adapter = adapter;

			this.Query = query;
			this.Owner = owner;
			this.Tags = tags;
			this.FactorioVersion = factorioVersion;
			this.Order = order;
		}

		/// <inheritdoc />
		public IEnumerator<ModInfoFull> GetEnumerator()
		{
			return new Enumerator(this);
		}

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}


		public class Enumerator : IEnumerator<ModInfoFull>
		{
			private readonly ModListEnumerable enumerable;
			private int page;
			private readonly Queue<ModInfoShort> cache;

			public Enumerator(ModListEnumerable enumerable)
			{
				this.enumerable = enumerable;
				this.page = 1;

				this.cache = new Queue<ModInfoShort>(25);
			}

			private bool RefillCache()
			{
				while (!this.cache.Any())
				{
					var list = this.enumerable.Adapter.GetModList(this.enumerable.Query, this.enumerable.Owner, this.enumerable.Tags, this.page, 25, this.enumerable.Order);
				    if (list == null || !list.Results.Any())
				        return false;
					this.page++;
					foreach (var mi in list.Results)
						this.cache.Enqueue(mi);
				    
				}
			    return true;
            }

			/// <inheritdoc />
			public void Dispose()
			{
			}

			/// <inheritdoc />
			public bool MoveNext()
			{


			    ModInfoFull full;
			    do
			    {
			        if (!this.RefillCache())
			            return false;

			        var first = this.cache.Dequeue();
			        full = first.GetFullInfo();

			    } while (!full.IsCompatible(this.enumerable.FactorioVersion));


			    this.Current = full;
				return true;
			}

			/// <inheritdoc />
			public void Reset()
			{
				this.cache.Clear();
				this.page = 1;
			}

			/// <inheritdoc />
			public ModInfoFull Current { get; private set; }

			/// <inheritdoc />
			object IEnumerator.Current => this.Current;

		}

	}
}
