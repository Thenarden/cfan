﻿using System;
using System.Collections.Generic;
using System.IO;
using CFAN.Properties;
using IniParser;
using IniParser.Model;

namespace CFAN.Factorio.Api
{
    public class GameConfig
    {
	    public const string PathReadDataValue = @"__PATH__executable__\..\..\data";
	    public const string PathWriteDataValue = @"__PATH__executable__\..\..";

		private static string IniFile(string folder) => Path.Combine(folder, @"config\config.ini");

	    public static GameConfig LoadOrGenerate(string folder)
		{
			var file = IniFile(folder);
			if (!File.Exists(file))
				return Generate(folder);

			return Load(folder);

		}

		public static GameConfig Load(string folder)
	    {
		    var file = IniFile(folder);
			if (!File.Exists(file))
				return new GameConfig();

		    var parser = new IniParser.FileIniDataParser();

		    var ini = parser.ReadFile(file);

			var res = new GameConfig();

			res.EnableNewMods = Boolean.Parse(ini["other"]["enable-new-mods"] ?? "true");
		    res.PathReadData = ini["path"]["read-data"];
		    res.PathWriteData = ini["path"]["write-data"];

			return res;

		}
	    public static GameConfig Generate(string folder)
	    {
			var ini = new IniData();
			var pathSection = new SectionData("path");
			pathSection.Comments.AddRange(new List<string>
			{
				" version=2",
				" This is INI file: https://en.wikipedia.org/wiki/INI_file#Format",
				" Semicolons (;) at the beginning of the line indicate a comment. Comment lines are ignored."
			});
			pathSection.Keys["read-data"] = @"__PATH__executable__\..\..\data";
			pathSection.Keys["write-data"] = @"__PATH__executable__\..\..";

			ini.Sections.Add(pathSection);

		    if (!Directory.Exists(Path.Combine(folder, "config")))
			    Directory.CreateDirectory(Path.Combine(folder, "config"));

		    var parser = new IniParser.FileIniDataParser();
		    parser.WriteFile(IniFile(folder), ini);
			return new GameConfig();
		}
		
	    public static void FixPathes(string folder)
	    {
		    var file = IniFile(folder);
			if (!File.Exists(file))
				return;

		    var lines = File.ReadAllLines(file);
		    bool isPathSegment = false;
		    for (int i = 0; i < lines.Length; i++)
		    {
			    var line = lines[i].Trim();

			    if (line.StartsWith("["))
			    {
				    isPathSegment = (line == "[path]");
			    }

			    if (isPathSegment && line.StartsWith("read-data") && line != "read-data=" + PathReadDataValue)
				    lines[i] = @"read-data=" + PathReadDataValue;
			    if (isPathSegment && line.StartsWith("write-data") && line != "read-data=" + PathWriteDataValue)
				    lines[i] = @"write-data=" + PathWriteDataValue;
			}
			
			File.WriteAllLines(file, lines);

	    }

	    public bool NeedsFix()
	    {
		    return (this.PathReadData != PathReadDataValue)
		           || (this.PathWriteData != PathWriteDataValue);
	    }

		public string PathReadData { get; set; }
	    public string PathWriteData { get; set; }

		public bool EnableNewMods { get; set; } = true;
    }
}
