﻿using System;

namespace CFAN.Factorio.Api
{
	[Flags]
	public enum LicenseFlags
	{
		CommercialUse = (1 << 0),
		Modification = (1 << 1),
		Distribution = (1 << 2),
		PatentUse = (1 << 3),
		PrivateUse = (1 << 4),

		DiscloseSource = (1 << 5),
		LicenseCopyrightNotice = (1 << 6),
		SameLicense = (1 << 7),
		StateChanges = (1 << 8),

		HoldLiable = (1 << 9),
		TrademarkUse = (1 << 10),
	}
}
