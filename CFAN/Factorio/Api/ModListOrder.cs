﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CFAN.Factorio.Api
{
	public enum ModListOrder
	{
		MostDownloaded,
		Alphabetically,
		RecentlyUpdated
	}


	public static class ModListOrderInfo
	{
		public static readonly IReadOnlyDictionary<ModListOrder, string> DisplayText = new ReadOnlyDictionary<ModListOrder, string>(
			new Dictionary<ModListOrder, string>
			{
				{ModListOrder.MostDownloaded, "Most Downloaded" },
				{ModListOrder.Alphabetically, "Alphabetically" },
				{ModListOrder.RecentlyUpdated, "Recently Updated" }
			});

		public static readonly IReadOnlyDictionary<ModListOrder, string> Query = new ReadOnlyDictionary<ModListOrder, string>(
			new Dictionary<ModListOrder, string>
			{
				{ModListOrder.MostDownloaded, "top" },
				{ModListOrder.Alphabetically, "alpha" },
				{ModListOrder.RecentlyUpdated, "updated" }
			});
	}
}
