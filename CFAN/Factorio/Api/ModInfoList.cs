﻿namespace CFAN.Factorio.Api
{
	public class ModInfoList
	{
		public Pagination Pagination { get; set; }


		public ModInfoShort[] Results { get; set; }
	}
}
