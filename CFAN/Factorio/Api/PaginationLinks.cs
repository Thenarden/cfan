﻿using System;

namespace CFAN.Factorio.Api
{
	public class PaginationLinks
	{
		public Uri First { get; set; }


		public Uri Prev { get; set; }


		public Uri Next { get; set; }


		public Uri Last { get; set; }
	}
}
