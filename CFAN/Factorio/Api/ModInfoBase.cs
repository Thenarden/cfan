﻿using System;
using System.Diagnostics;
using CFAN.Factorio.Adapter;
using Newtonsoft.Json;


namespace CFAN.Factorio.Api
{
	[JsonObject(MemberSerialization.OptOut)]
	[DebuggerDisplay("Name={" + nameof(Name) + "}, Title={" + nameof(Title) + "}")]
	public abstract class ModInfoBase : IEquatable<ModInfoBase>

	{
		public static readonly Uri Github = new Uri("https://github.com/");

		protected ModInfoBase(IModInfoAdapter adapter, int id, string name)
		{
			Id = id;
			Name = name;
			Adapter = adapter;
		}


		[JsonIgnore]
		public IModInfoAdapter Adapter { get;}


		public int Id { get; }


		public string Name { get; }


		public string Title { get; set; }


		public string Owner { get; set; }


		public string Summary { get; set; }


		public DateTimeOffset CreatedAt { get; set; }


		public DateTimeOffset UpdatedAt { get; set; }


		public int DownloadsCount { get; set; }


		public string[] GameVersions { get; set; }


		public Uri GithubPath { get; set; }


		public Uri GithubPathFull
		{
			get => this.GithubPath != null ? new Uri(Github, this.GithubPath) : null;
			set => this.GithubPath = value.MakeRelativeUri(Github);
		}


		public Uri Homepage { get; set; }


		public LicenseFlags LicenseFlags { get; set; }

		public string LicenseName { get; set; }


		public string LicenseUrl { get; set; }


		public Tag[] Tags { get; set; }


		public bool Equals(ModInfoBase other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return Id == other.Id && string.Equals(Name, other.Name);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((ModInfoBase) obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				return (Id * 397) ^ (Name != null ? Name.GetHashCode() : 0);
			}
		}

		public virtual ModInfoFull GetFullInfo()
		{
			return this.Adapter.GetModInfo(this.Name);
		}


		public string GetFilenameForVersion(Version version, bool archive=true) => $"{this.Name}_{version}" + (archive ? ".zip" : "");
	}
}
