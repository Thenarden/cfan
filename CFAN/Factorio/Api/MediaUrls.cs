﻿using System;

namespace CFAN.Factorio.Api
{
	public class MediaUrls
	{
		public Uri Original { get; set; }


		public Uri Thumb { get; set; }
	}
}
