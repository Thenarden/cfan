﻿using System.Diagnostics;
using CFAN.Factorio.Adapter;

namespace CFAN.Factorio.Api
{
	[DebuggerDisplay("Name={" + nameof(Name) + "}, Title={"+nameof(Title)+"}")]
	public class ModInfoShort : ModInfoBase
	{
		public Release LatestRelease { get; set; }


		public ModInfoShort(IModInfoAdapter adapter, int id, string name) : base(adapter, id, name)
		{
		}


	}
}
