﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CFAN.Factorio.Api
{
	public class ModInfoFile
	{
		public string Name { get; set; }

		public Version Version { get; set; }

		public string Title { get; set; }

		public string Author { get; set; }

		public string Homepage { get; set; }

		public List<String> Dependencies { get; set; }

		public string Description { get; set; }

		public Version FactorioVersion { get; set; }
	}
}
