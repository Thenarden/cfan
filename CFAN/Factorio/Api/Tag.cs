﻿namespace CFAN.Factorio.Api
{
	public class Tag
	{
		public int Id { get; set; }


		public string Type { get; set; }


		public string Name { get; set; }


		public string Title { get; set; }


		public string Description { get; set; }
	}
}
