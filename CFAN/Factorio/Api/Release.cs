﻿using System;
using System.Diagnostics;
using Newtonsoft.Json.Linq;

namespace CFAN.Factorio.Api
{
	[DebuggerDisplay("Version={" + nameof(Version) + "}, FactorioVersion={" + nameof(FactorioVersion) + "}")]
	public class Release
	{
		public static readonly Uri DownloadRoot = new Uri("https://mods.factorio.com/");

		public int Id { get; set; }


		public Version Version { get; set; }


		public Version FactorioVersion { get; set; }


		public Version GameVersion { get; set; }


		public Uri DownloadUrl { get; set; }


		public Uri FullDownloadUrl
		{

			get => this.DownloadUrl != null ? new Uri(DownloadRoot, this.DownloadUrl) : null;
			set => this.DownloadUrl = value.MakeRelativeUri(DownloadRoot);
		}


		public int DownloadsCount { get; set; }


		public string FileName { get; set; }


		public int FileSize { get; set; }


		public DateTimeOffset ReleasedAt { get; set; }


		public JObject InfoJson { get; set; }
	}
}
