﻿namespace CFAN.Factorio.Api
{
	public class MediaFile
	{

		public int Id { get; set; }


		public int Width { get; set; }


		public int Height { get; set; }


		public int Size { get; set; }


		public MediaUrls Urls { get; set; }

	}
}
