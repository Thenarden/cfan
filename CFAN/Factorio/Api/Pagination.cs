﻿namespace CFAN.Factorio.Api
{
	public class Pagination
	{
		public int Count { get; set; }


		public int Page { get; set; }


		public int PageCount { get; set; }


		public int PageSize { get; set; }


		public PaginationLinks Links { get; set; }
	}
}
