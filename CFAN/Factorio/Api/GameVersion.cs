﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using CFAN.Factorio.Adapter;
using CFAN.Model;
using Shortbit.Utils;

namespace CFAN.Factorio.Api
{
	[DebuggerDisplay("{"+nameof(Version)+ "}" )]
	public class GameVersion : IEquatable<GameVersion>, IComparable<GameVersion>
	{

		public class Urls
		{
			public Urls()
			{
				Platforms = new Dictionary<Platform, Uri>();
			}

			public Uri Win32
			{
				get => this.Platforms.GetOrDefault(Platform.Win32);
				set => this.Platforms[Platform.Win32] = value;
			}
			
			public Uri Win64
			{
				get => this.Platforms.GetOrDefault(Platform.Win64);
				set => this.Platforms[Platform.Win64] = value;
			}

			public Uri Osx
			{
				get => this.Platforms.GetOrDefault(Platform.Osx);
				set => this.Platforms[Platform.Osx] = value;
			}

			public Uri Linux64
			{
				get => this.Platforms.GetOrDefault(Platform.Linux64);
				set => this.Platforms[Platform.Linux64] = value;
			}
			
			public Uri HeadlessLinux64
			{
				get => this.Platforms.GetOrDefault(Platform.HeadlessLinux64);
				set => this.Platforms[Platform.HeadlessLinux64] = value;
			}

			public Dictionary<Platform, Uri> Platforms { get; }
		}


		public Version Version { get; }


		public Version MainVersion => new Version(this.Version.Major, this.Version.Minor);


		public Urls Downloads { get; }


		public bool Experimental { get; set; }


		public IGameVersionAdapter Adapter { get; }

		public GameVersion(Version version, IGameVersionAdapter adapter)
		{
			this.Version = version;
			Adapter = adapter;
			this.Downloads = new Urls();

		}


		public string GetFilenameForPlatform(Platform platform)
		{
			return $"Factorio_{this.Version}_{platform}" + PlatformInfo.Extension[platform];
		}

		public string GetArchiveRootFolderName() => $"Factorio_{this.Version}";


		public bool Equals(GameVersion other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return Equals(Version, other.Version);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((GameVersion) obj);
		}

		public override int GetHashCode()
		{
			return (Version != null ? Version.GetHashCode() : 0);
		}

		public int CompareTo(GameVersion other)
		{
			if (ReferenceEquals(this, other)) return 0;
			if (ReferenceEquals(null, other)) return 1;
			return Comparer<Version>.Default.Compare(Version, other.Version);
		}
	}
}
