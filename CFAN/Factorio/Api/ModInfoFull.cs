﻿using System;
using System.Diagnostics;
using System.Linq;
using CFAN.Factorio.Adapter;
using MoreLinq;
using Newtonsoft.Json;

namespace CFAN.Factorio.Api
{
	public class ModInfoFull : ModInfoBase
	{

		public string Description { get; set; }


		public string DescriptionHtml { get; set; }


		public MediaFile[] MediaFiles { get; set; }


		public Release[] Releases { get; set; }

        
	    public Release LatestMatchingRelease (Version factorioVersion)
	    {
	        if (factorioVersion == null)
	            return null;

	        try
	        {
	            return this.Releases.Where(r => r.FactorioVersion == factorioVersion).MaxBy(r => r.Version);
	        }
	        catch (InvalidOperationException)
	        {
	            return null;
	        }
	    }

	    public bool IsCompatible(Version factorioVersion)
	    {
	        if (factorioVersion == null)
	            return true;
	        return this.Releases.Any(r => r.FactorioVersion == factorioVersion);
	    }


		public ModInfoFull(IModInfoAdapter adapter, int id, string name) : base(adapter, id, name)
		{
		}

		public override ModInfoFull GetFullInfo()
		{
			return this;
		}
	}
}
