﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace CFAN
{
	public static class Extensions
	{

		public static void SetZero(this char[] self)
		{
			for (int i = 0; i < self.Length; i++)
				self[i] = '\0';
		}

		public static string GetResponseContent(this HttpWebResponse self)
		{
			using (var s = self.GetResponseStream())
			using (var r = new StreamReader(s))
			{
				return r.ReadToEnd();
			}
		}

		public static T FindChildOfType<T>(this DependencyObject root) where T : class
		{
			var queue = new Queue<DependencyObject>();
			queue.Enqueue(root);

			while (queue.Count > 0)
			{
				DependencyObject current = queue.Dequeue();
				for (int i = VisualTreeHelper.GetChildrenCount(current) - 1; 0 <= i; i--)
				{
					var child = VisualTreeHelper.GetChild(current, i);
					var typedChild = child as T;
					if (typedChild != null)
					{
						return typedChild;
					}
					queue.Enqueue(child);
				}
			}
			return null;
		}

		public static IEnumerable<ZipArchiveEntry> EntriesInFolder(this ZipArchive self, string folder)
		{
			return self.Entries.Where(e => e.FullName.StartsWith(folder));
		}

		private static string GetHumanDiffPhrase(this TimeSpan time)
		{
			int days = Math.Abs(time.Days);
			int hours = Math.Abs(time.Hours);
			int minutes = Math.Abs(time.Minutes);

			int weeks = days / 7;
			int months = days / 30;
			int years = days / 365;

			if (years > 1)
				return $"{years} years";
			if (years == 1)
				return "one year";

			if (months > 1)
				return $"{months} months";

			if (weeks > 1)
				return $"{weeks} weeks";
			if (weeks == 1)
				return "one week";

			if (days > 1)
				return $"{days} days";
			if (days == 1)
				return "one day";

			if (hours > 1)
				return $"{hours} hours";
			if (hours == 1)
				return "one hour";

			if (minutes > 1)
				return $"{minutes} minutes";
			if (minutes == 1)
				return "one minute";

			return time.ToString();

		}
		public static string ToHumanTimeDiff(this TimeSpan time)
		{
			if (time.Minutes == 0)
				return "just now";

			var phrase = GetHumanDiffPhrase(time);

			if (time.TotalSeconds < 0)
				return phrase + " ago";
			return "in " + phrase;
		}

        public static string ToHumanReadable(this short me)
        {
            var units = new[] { "", "k", "M", "B", "T" };

            var abs = Math.Abs(me);
            int order = 0;
            while (abs >= 1000 && order < units.Length - 1)
            {
                order++;
                abs = (short)(abs / 1000);
            }

            return $"{(me < 0 ? "neg. " : "")}{abs:0.##}{units[order]}";
        }
        public static string ToHumanReadable(this int me)
	    {
	        var units = new[] { "", "k", "M", "B", "T"};

	        var abs = Math.Abs(me);
	        int order = 0;
	        while (abs >= 1000 && order < units.Length - 1) {
	            order++;
	            abs = abs/1000;
	        }

	        return $"{(me < 0 ? "neg. " : "")}{abs:0.##}{units[order]}";
        }
        public static string ToHumanReadable(this long me)
        {
            var units = new[] { "", "k", "M", "B", "T" };

            var abs = Math.Abs(me);
            int order = 0;
            while (abs >= 1000 && order < units.Length - 1)
            {
                order++;
                abs = abs / 1000;
            }

            return $"{(me < 0 ? "neg. " : "")}{abs:0.##}{units[order]}";
        }
        public static string ToHumanReadableSize(this short me)
        {
            var units = new[] { "B", "kB", "MB", "GB", "TB" };

            var abs = Math.Abs(me);
            int order = 0;
            while (abs >= 1024 && order < units.Length - 1)
            {
                order++;
                abs = (short)(abs / 1024);
            }

            return $"{(me < 0 ? "neg. " : "")}{abs:0.##}{units[order]}";
        }
        public static string ToHumanReadableSize(this int me)
        {
            var units = new[] { "B", "kB", "MB", "GB", "TB" };

            var abs = Math.Abs(me);
            int order = 0;
            while (abs >= 1024 && order < units.Length - 1)
            {
                order++;
                abs = abs / 1024;
            }

            return $"{(me < 0 ? "neg. " : "")}{abs:0.##}{units[order]}";
        }
        public static string ToHumanReadableSize(this long me)
        {
            var units = new[] { "B", "kB", "MB", "GB", "TB" };

            var abs = Math.Abs(me);
            int order = 0;
            while (abs >= 1024 && order < units.Length - 1)
            {
                order++;
                abs = abs / 1024;
            }

            return $"{(me < 0 ? "neg. " : "")}{abs:0.##}{units[order]}";
        }

        public static string ToYesNo(this bool me)
	    {
	        if (me)
	            return "Yes";
	        return "No";
	    }


		public static string GetContent(this ZipArchiveEntry self)
		{
			using (var stream = self.Open())
			using (var reader = new StreamReader(stream))
			{
				return reader.ReadToEnd();
			}
		}
		public static void SetContent(this ZipArchiveEntry self, string content)
		{
			using (var target = self.Open())
			using (var writer = new StreamWriter(target))
			{
				writer.Write(content);
			}
		}

		public static void CopyTo(this ZipArchiveEntry self, Stream target)
		{
			using (var s = self.Open())
			{
				s.CopyTo(target);
			}
		}
		public static void CopyToFile(this ZipArchiveEntry self, string file)
		{
			using(var stream = new FileStream(file, FileMode.Create))
			using (var s = self.Open())
			{
				s.CopyTo(stream);
			}
		}

		public static void CopyFrom(this ZipArchiveEntry self, Stream source)
		{
			using (var target = self.Open())
			{
				source.CopyTo(target);
			}
		}

		public static void CopyFromFile(this ZipArchiveEntry self, string file)
		{
			using (var source = new FileStream(file, FileMode.Open))
			using (var target = self.Open())
			{
				source.CopyTo(target);
			}
		}
	}
}
