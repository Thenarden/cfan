﻿using System;
using System.Windows;

namespace CFAN
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{

		public AppContext Context { get; }

		public App()
		{
			throw new NotSupportedException();
		}

		public App(AppContext context)
		{
			Context = context;
		}
	}
}
