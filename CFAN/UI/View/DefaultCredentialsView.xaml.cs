﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CFAN.UI.ViewModel;

namespace CFAN.UI.View
{
	/// <summary>
	/// Interaction logic for DefaultCredentialsView.xaml
	/// </summary>
	public partial class DefaultCredentialsView : UserControl
	{
		public DefaultCredentialsViewModel ViewModel => (DefaultCredentialsViewModel) this.DataContext;

		public DefaultCredentialsView()
		{
			InitializeComponent();
		}

		private void PasswordBox_OnPasswordChanged(object sender, RoutedEventArgs e)
		{
			this.ViewModel.Password = this.PasswordBox.SecurePassword;
		}
	}
}
