﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CFAN.UI.ViewModel;
using Microsoft.Win32;

namespace CFAN.UI.View
{
	/// <summary>
	/// Interaction logic for ExportProfileView.xaml
	/// </summary>
	public partial class ExportProfileView : UserControl
	{
		public ExportProfileViewModel Model => (ExportProfileViewModel) this.DataContext;

		public ExportProfileView()
		{
			InitializeComponent();
		}

		private void PathSelectButton_Click(object sender, RoutedEventArgs e)
		{
			var diag = new SaveFileDialog();
			diag.DefaultExt = "zip";
			diag.Filter = "Zip Archive (*.zip)|*.zip";
			diag.InitialDirectory = this.Model.Application.Context.Settings.LastExportFolder;

			if (diag.ShowDialog() == true)
				this.PathBox.Text = diag.FileName;
		}
	}
}
