﻿using System;
using System.Windows;
using System.Windows.Controls;
using CFAN.UI.ViewModel;

namespace CFAN.UI.View
{
	/// <summary>
	/// Interaction logic for ProfilesListView.xaml
	/// </summary>
	public partial class ProfilesListView : UserControl
	{
		public static readonly DependencyProperty ModelProperty = DependencyProperty.Register("Model", typeof(ProfileListViewModel), typeof(ProfilesListView));

		public ProfileListViewModel Model
		{
			get => (ProfileListViewModel)this.GetValue(ModelProperty);
			set => this.SetValue(ModelProperty, value);
		}

		public ProfilesListView()
		{
			InitializeComponent();
			Console.WriteLine();
		}
		
	}
}
