﻿using System.Windows;
using System.Windows.Controls;
using CFAN.UI.ViewModel;

namespace CFAN.UI.View
{
	/// <summary>
	/// Interaction logic for DeleteProfileView.xaml
	/// </summary>
	public partial class DeleteProfileView : UserControl
	{

		public DeleteProfileViewModel Model
		{
			get => (DeleteProfileViewModel)this.DataContext;
			set => this.DataContext = value;
		}

		public DeleteProfileView()
		{
			InitializeComponent();
		}
	}
}
