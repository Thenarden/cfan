﻿using System;
using System.Windows;
using System.Windows.Input;

namespace CFAN.UI.StyleableWindow
{
    public class WindowMinimizeCommand :ICommand
    {     

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            var window = parameter as Window;

            if (window != null)
            {
                window.WindowState = WindowState.Minimized;
            }
        }

	    protected virtual void OnCanExecuteChanged()
	    {
		    CanExecuteChanged?.Invoke(this, EventArgs.Empty);
	    }
    }
}
