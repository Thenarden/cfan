﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CFAN.Tasks;
using CFAN.UI.ViewModel;
using CFAN.Utils;

namespace CFAN.UI
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : CustomWindow, INotifyPropertyChanged
	{
		public App Application => (App)System.Windows.Application.Current;


		public ICommand ShowViewCommand { get; }
		public ICommand CloseViewCommand { get; }
		public ICommand ShowPopupCommand { get; }
		public ICommand ClosePopupCommand { get; }

		public ICommand CloseActiveCommand { get; }



		public event Action OnExit = delegate { };

		
		public ContentViewModel ActiveContent => (ContentViewModel) this.Popup ?? this.View;

		private ViewContentViewModel view = new ProfileListViewModel();
		public ViewContentViewModel View
		{
			get => view;
			set
			{
				view = value;
				view.Window = this;
			}
		}

		private PopupContentViewModel popup = null;
		public PopupContentViewModel Popup
		{
			get => popup;
			set
			{
				popup = value;
				if (popup != null)
					popup.Window = this;
			}
		}

        public ObservableCollection<BackgroundTaskViewModel> RunningTasks { get; }

		private readonly Stack<ViewContentViewModel> viewHistory = new Stack<ViewContentViewModel>();
		private readonly Stack<PopupContentViewModel> popupHistory = new Stack<PopupContentViewModel>();


		public MainWindow()
		{
			this.ShowViewCommand = new SimpleCommand<ViewContentViewModel>(this.ShowView);
			this.CloseViewCommand = new SimpleCommand(o => this.CloseView());
			this.ShowPopupCommand = new SimpleCommand<PopupContentViewModel>(this.ShowPopup);
			this.ClosePopupCommand = new SimpleCommand(o => this.ClosePopup());

			this.CloseActiveCommand = new SimpleCommand(o => this.CloseActive());

            this.Application.Context.Tasks.CollectionChanged += this.Tasks_OnCollectionChanged;
            this.RunningTasks = new ObservableCollection<BackgroundTaskViewModel>(
                this.Application.Context.Tasks.Select(t => t is CompositeTask ct
                                                          ? new CompositeTaskViewModel(ct, this.Dispatcher)
                                                          : new BackgroundTaskViewModel(t, this.Dispatcher)));

			InitializeComponent();

		}

        private void Tasks_OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs args)
        {
            if (!this.Dispatcher.CheckAccess())
            {
                this.Dispatcher.InvokeAsync(() => this.Tasks_OnCollectionChanged(sender, args));
                return;
            }

            if (args.Action == NotifyCollectionChangedAction.Reset)
            {
                this.RunningTasks.Clear();
                foreach (var subTask in this.Application.Context.Tasks)
                {
                    var vm = subTask is CompositeTask task
                        ? new CompositeTaskViewModel(task, this.Dispatcher)
                        : new BackgroundTaskViewModel(subTask, this.Dispatcher);

                    this.RunningTasks.Add(vm);
                }

            }

            if (args.Action == NotifyCollectionChangedAction.Remove ||
                args.Action == NotifyCollectionChangedAction.Replace)
            {
                foreach (BackgroundTask subTask in args.OldItems)
                {
                    var vm = this.RunningTasks.First(v => v.RelatedTask == subTask);
                    this.RunningTasks.Remove(vm);
                }
            }

            if (args.Action == NotifyCollectionChangedAction.Add ||
                args.Action == NotifyCollectionChangedAction.Replace)
            {
                var index = args.NewStartingIndex;
                foreach (BackgroundTask subTask in args.NewItems)
                {
                    var vm = subTask is CompositeTask task
                        ? new CompositeTaskViewModel(task, this.Dispatcher)
                        : new BackgroundTaskViewModel(subTask, this.Dispatcher);

                    this.RunningTasks.Insert(index, vm);
                    index++;
                }
            }
        }

        public void ShowView(ViewContentViewModel newContent)
		{
			this.ShowView(newContent, true);
		}
		public void ShowView(ViewContentViewModel newContent, bool appendHistory)
		{
			if (appendHistory && this.View != null)
				this.viewHistory.Push(this.View);
			this.View = newContent;
		}

		public void CloseView()
		{
			if (this.viewHistory.Any())
				this.View = this.viewHistory.Pop();
		}

		public void ClearViewHistory()
		{
			this.viewHistory.Clear();
		}

		public void ShowPopup(PopupContentViewModel newContent)
		{
			this.ShowPopup(newContent, true);
		}
		public void ShowPopup(PopupContentViewModel popup, bool appendHistory)
		{
			if (appendHistory && this.Popup != null)
				this.popupHistory.Push(this.Popup);
			this.Popup = popup;
		}

		public void ClosePopup()
		{
			if (this.popupHistory.Any())
				this.Popup = this.popupHistory.Pop();
			else
				this.Popup = null;
		}

		public void ClearPopupHistory()
		{
			this.popupHistory.Clear();
		}

		public void CloseActive()
		{
			if (this.Popup != null)
				this.ClosePopup();
			else
				this.CloseView();
		}


		private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			e.Cancel = true;
			Visibility = Visibility.Hidden;
			OnExit();
		}

		private void Menu_Close_Click(object sender, RoutedEventArgs e)
		{
			this.Close();
		}
		private void Menu_Exit_Click(object sender, RoutedEventArgs e)
		{
			this.Close();
			OnExit();
		}

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
		{
			PropertyChanged?.Invoke(this, e);
		}

		private void PopupBorder_OnMouseDown(object sender, MouseButtonEventArgs e)
		{
			e.Handled = true;
		}

		private void CommandBinding_OnExecuted(object sender, ExecutedRoutedEventArgs e)
		{
			if (this.ActiveContent != null && this.ActiveContent.AllowBackButton)
				this.CloseActive();
		}
	}
}
