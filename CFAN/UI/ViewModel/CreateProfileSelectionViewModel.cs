﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using CFAN.Utils;
using Microsoft.Win32;

namespace CFAN.UI.ViewModel
{
    public class CreateProfileSelectionViewModel : PopupContentViewModel,INotifyPropertyChanged
    {
		

		public bool IsSteamEnabled { get; set; }



		public ICommand CreateCommand { get; set; }
	    public ICommand ImportArchiveCommand { get; set; }
	    public ICommand ImportFolderCommand { get; set; }
	    public ICommand ImportSteamCommand { get; set; }


		public CreateProfileSelectionViewModel()
	    {
		    this.CreateCommand = new SimpleCommand(o =>
		    {
				this.ClosePopup();
				this.ShowPopup(new CreateProfileViewModel());
		    });

			this.ImportArchiveCommand = new SimpleCommand(o =>
			{
				this.ClosePopup();

				var diag = new OpenFileDialog();
				diag.DefaultExt = "zip";
				diag.Filter = "Zip Archive (*.zip)|*.zip";
				diag.InitialDirectory = this.Application.Context.Settings.LastExportFolder;

				if (diag.ShowDialog() != true)
					return;

				// TODO: Check for duplicates
				var manifest = this.Application.Context.Profile.GetManifestFromArchive(diag.FileName);
				this.ShowPopup(new CreateFromExportViewModel(diag.FileName, manifest));
			});
	    }


	    public event PropertyChangedEventHandler PropertyChanged;
		protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
	    {
		    PropertyChanged?.Invoke(this, e);
	    }
    }
}
