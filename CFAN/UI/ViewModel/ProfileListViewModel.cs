﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using CFAN.Model;
using CFAN.Utils;

namespace CFAN.UI.ViewModel
{
    public class ProfileListViewModel : ViewContentViewModel
    {
		

	    private readonly ItemCollectionWithSpecialItems items;
	    public ObservableCollection<ProfileListItemViewModel> Items => items;

	    private readonly INotifyCollectionChanged collectionChangedHandle;

	    private readonly SimpleCommand<ProfileListItemViewModel> itemClickCommand;
		
		public ProfileListViewModel() 
		{
			this.itemClickCommand = new SimpleCommand<ProfileListItemViewModel>(item => this.ShowView(new ProfileViewModel(item.RelatedProfile), true));

			var addNewItemViewModel = new ProfileListItemViewModel
			{
				Icon = Properties.Resources.Add_outlined,
				HoverIcon = Properties.Resources.Add,
				Title = "Add Profile",
				LeftClick = new SimpleCommand<ProfileListItemViewModel>(model => this.ShowPopup(new CreateProfileSelectionViewModel()))
			};

			this.items = new ItemCollectionWithSpecialItems(this, addNewItemViewModel);
		}
		private ProfileListViewModel(IEnumerable<Profile> source, INotifyCollectionChanged collectionChangedHandle)
			: this()
	    {
		    this.collectionChangedHandle = collectionChangedHandle;
			this.collectionChangedHandle.CollectionChanged += CollectionChangedHandleOnCollectionChanged;

			foreach(var i in source.Select(this.CreateItem))
				this.items.Add(i);
		}

	    public static ProfileListViewModel FromObservable<T>(T source) where T: IEnumerable<Profile>, INotifyCollectionChanged
	    {
		    return new ProfileListViewModel(source, source);
	    }

	    private ProfileListItemViewModel CreateItem(Profile profile)
	    {
		    return new ProfileListItemViewModel(profile)
		    {
			    LeftClick = itemClickCommand
			};
	    }


	    private void CollectionChangedHandleOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs args)
	    {
		    ProfileListItemViewModel item;

			switch (args.Action)
			{
				case NotifyCollectionChangedAction.Reset:
					this.Items.Clear();
					break;

				case NotifyCollectionChangedAction.Add:
					foreach (Profile profile in args.NewItems)
					{
						this.Items.Add(this.CreateItem(profile));
					}
					break;
			    case NotifyCollectionChangedAction.Remove:
				    foreach (Profile profile in args.OldItems)
				    {
					    item = this.Items.FirstOrDefault(i => i.RelatedProfile == profile);
					    this.Items.Remove(item);
				    }
				    break;

				case NotifyCollectionChangedAction.Replace:
					Profile oldProfile = (Profile)args.OldItems[0];
					Profile newProfile = (Profile)args.NewItems[0];

					item = this.Items.FirstOrDefault(i => i.RelatedProfile == oldProfile);
					var index = this.Items.IndexOf(item);
					this.Items.RemoveAt(index);
					this.Items.Insert(index, this.CreateItem(newProfile));

					break;
			}
	    }

	    protected override void DisposeManaged()
	    {
		    base.DisposeManaged();

			if (this.collectionChangedHandle != null)
			    this.collectionChangedHandle.CollectionChanged -= this.CollectionChangedHandleOnCollectionChanged;

		    foreach (var item in this.Items)
		    {
			    item.Dispose();
		    }
	    }



	    private class ItemCollectionWithSpecialItems : ObservableCollection<ProfileListItemViewModel>
	    {
		    public ProfileListItemViewModel AddNew { get; }
		    public ProfileListViewModel Model { get; }

		    public ItemCollectionWithSpecialItems(ProfileListViewModel model, ProfileListItemViewModel addNew)
			    : this(model, addNew, new List<ProfileListItemViewModel>())
		    { }

		    public ItemCollectionWithSpecialItems(ProfileListViewModel model, ProfileListItemViewModel addNew, IEnumerable<ProfileListItemViewModel> existing)
			    : base(existing)
		    {
			    this.Model = model;
			    this.AddNew = addNew;
			    this.Items.Add(AddNew);
		    }

		    private void ExecuteInDispacter(Action action)
		    {
			    if (this.Model.Window != null && !this.Model.Window.Dispatcher.CheckAccess())
				    this.Model.Window.Dispatcher.Invoke(action);
			    else
				    action();
		    }


		    public void AddExisting(IEnumerable<ProfileListItemViewModel> existing)
		    {
			    foreach (var e in existing)
				{
					this.Items.Add(e);
				}
		    }

		    protected override void ClearItems()
		    {
			    ExecuteInDispacter(() =>
			    {
				    base.ClearItems();
				    this.Items.Add(AddNew);
			    });
		    }

		    protected override void InsertItem(int index, ProfileListItemViewModel item)
		    {
			    if (index == this.Count)
				    index--;

			    ExecuteInDispacter(() => base.InsertItem(index, item));
		    }

		    protected override void RemoveItem(int index)
		    {
			    if (index == this.Count - 1)
				    return;
			    ExecuteInDispacter(() => base.RemoveItem(index));
		    }

		    protected override void SetItem(int index, ProfileListItemViewModel item)
		    {
			    if (index == this.Count - 1)
				    return;
			    ExecuteInDispacter(() => base.SetItem(index, item));
		    }

		    protected override void MoveItem(int oldIndex, int newIndex)
		    {
			    if (oldIndex == this.Count - 1 || newIndex == this.Count)
				    return;

			    ExecuteInDispacter(() => base.MoveItem(oldIndex, newIndex));
		    }
	    }
	}
}
