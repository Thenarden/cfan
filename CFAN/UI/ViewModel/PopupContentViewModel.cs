﻿using System;
using System.Windows;
using System.Windows.Input;
using CFAN.Utils;

namespace CFAN.UI.ViewModel
{
    public abstract class PopupContentViewModel : ContentViewModel
    {
	    private ICommand outOfBorderClick;
	    public ICommand OutOfBorderClick
	    {
		    get
		    {
			    if (this.AllowOutOfBorderClick)
					return outOfBorderClick;
			    return null;
		    }
		    set => outOfBorderClick = value;
	    }

	    public bool AllowOutOfBorderClick { get; set; } = true;


	    protected PopupContentViewModel()
	    {
		    this.outOfBorderClick = this.ClosePopupCommand;
	    }
	}
}
