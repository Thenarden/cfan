﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using CFAN.Utils;
using PropertyChanged;
using Shortbit.Utils;

namespace CFAN.UI.ViewModel
{
    public class CreateProfileViewModel : PopupContentViewModel, INotifyPropertyChanged
	{

		private string name;
		public string Name
	    {
		    get => name;
		    set
			{
				if (!this.pathChanged)
				{
					this.path = System.IO.Path.Combine(this.Application.Context.Settings.DefaultProfileFolder, value);
				}
				name = value;
		    }
	    }


		private string path;
		[DependsOn(nameof(Name))]
	    public string Path
	    {
		    get => path;
		    set
		    {
			    path = value;
			    this.pathChanged = true;
		    }
		}


		public string Description { get; set; }

		public GameVersionViewModel Version { get; set; }

		public ObservableCollection<GameVersionViewModel> AvailableVersions { get; set; }


		public ICommand ConfirmCommand { get; }

		private bool pathChanged = false;

		public CreateProfileViewModel()
		{
			this.path = this.Application.Context.Settings.DefaultProfileFolder;

            var versions = this.Application.Context.GameVersionAdapter.ListGameVersions().ToListSafe();
            versions.Sort();

            this.AvailableVersions =
				new ObservableCollection<GameVersionViewModel>(versions.Select(gv => new GameVersionViewModel(gv)));
			this.Version = ((IEnumerable<GameVersionViewModel>)this.AvailableVersions).Max();

			this.ConfirmCommand = new SimpleCommand(async o =>
			{
				if (string.IsNullOrWhiteSpace(this.Name))
					return;

				this.ClosePopup();

				var profile = await this.Application.Context.Profile.Create(this.Name, this.Path, this.Version.GameVersion);
				profile.Description = this.Description;

				
			});
		}
		


		public event PropertyChangedEventHandler PropertyChanged;

	    protected void OnPropertyChanged(PropertyChangedEventArgs e)
	    {
		    PropertyChanged?.Invoke(this, e);
	    }
    }
}
