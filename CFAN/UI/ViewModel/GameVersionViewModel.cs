﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CFAN.Factorio.Api;

namespace CFAN.UI.ViewModel
{
    public class GameVersionViewModel : IComparable<GameVersionViewModel>
    {
	    public GameVersionViewModel(GameVersion gameVersion)
	    {
		    GameVersion = gameVersion;
	    }

	    public GameVersion GameVersion { get; }

	    public Version Version => GameVersion.Version;

	    public bool IsExperimental => GameVersion.Experimental;
	    //public bool IsAlpha => GameVersion.


	    public override string ToString()
	    {
		    return this.Version.ToString();
	    }


	    public int CompareTo(GameVersionViewModel other)
	    {
		    if (ReferenceEquals(this, other)) return 0;
		    if (ReferenceEquals(null, other)) return 1;
		    return Comparer<GameVersion>.Default.Compare(GameVersion, other.GameVersion);
	    }
    }
}
