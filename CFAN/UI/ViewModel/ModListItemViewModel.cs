﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using CFAN.Factorio.Api;
using CFAN.Properties;
using CFAN.Utils;
using MoreLinq;

namespace CFAN.UI.ViewModel
{
	public class ModListItemViewModel : INotifyPropertyChanged
	{
		
		public string Title { get; set; }

		public string Owner { get; set; }

		public string Summary { get; set; }

		public DateTime LastUpdate { get; set; }

		public int Downloads { get; set; }

		public Version FactorioVersion { get; set; }
	    
	    public Version ModVersion { get; set; }

        
	    public Image Icon { get; set; } = Resources.FactorioGear;


	    public bool IsInstalled { get; set; }

	    public bool IsCompatible { get; set; }


		public String TxtLastUpdate => $"Last updated: {(LastUpdate - DateTime.Now).ToHumanTimeDiff()}";
		public String TxtDownloads => $"Downloads: {this.Downloads.ToHumanReadable()}";
		public String TxtOwner => $"by {this.Owner}";
		public String TxtFactorioVersion => $"for Factorio {this.FactorioVersion}";
	    public String TxtModVersion => $"Latest Version {this.ModVersion}";
	    public String TxtIsCompatible => $"Compatible: {this.IsCompatible.ToYesNo()}";

		public String TxtInstallButton => this.IsInstalled ? "Already installed" : "Install";
		public bool InstallButtonEnabled => !this.IsInstalled;



		public ModInfoFull Mod { get;}


		public ICommand InstallCommand { get; }
	    public ICommand AuthorSearchCommand { get; }


		public ModListItemViewModel(ModInfoFull mod, bool isInstalled, bool isCompatible, SimpleCommand<ModListItemViewModel> installCommand, SimpleCommand<string> authorSearchCommand)
		{
			this.Mod = mod;

			this.Title = mod.Title;
			this.Owner = mod.Owner;
			this.Summary = mod.Summary;
			this.LastUpdate = mod.UpdatedAt.DateTime;
			this.Downloads = mod.DownloadsCount;
		    this.ModVersion = mod.Releases.MaxBy(r => r.Version).GameVersion;
			this.FactorioVersion = mod.Releases.MaxBy(r => r.Version).FactorioVersion;

			this.IsInstalled = isInstalled;
		    this.IsCompatible = isCompatible;

		    this.AuthorSearchCommand = authorSearchCommand;

			if (isInstalled)
				this.InstallCommand = new SimpleCommand();
			else
				this.InstallCommand = installCommand;
		}

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
