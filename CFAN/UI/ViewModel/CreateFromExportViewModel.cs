﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using CFAN.Model;
using CFAN.Utils;
using PropertyChanged;

namespace CFAN.UI.ViewModel
{
	public class CreateFromExportViewModel : PopupContentViewModel, INotifyPropertyChanged
	{
		private string name;
		public string Name
		{
			get => name;
			set
			{
				if (!this.pathChanged)
				{
					this.path = defaultPath(value); 
				}
				name = value;
			}
		}


		private string path;
		[DependsOn(nameof(Name))]
		public string Path
		{
			get => path;
			set
			{
				path = value;
				this.pathChanged = true;
			}
		}


		public string SourceFile { get; }

		public ICommand ConfirmCommand { get; }

		private bool pathChanged = false;

		public CreateFromExportViewModel(string sourceFile, ProfileManifest manifest)
		{
			this.SourceFile = sourceFile;
			this.Name = manifest.Name;
			this.path = defaultPath(this.Name);
			this.pathChanged = false;

			this.ConfirmCommand = new SimpleCommand(o =>
			{
				this.ClosePopup();
#pragma warning disable 4014
                Task.Run(async ()  => await this.Application.Context.Profile.CreateFromExport(this.SourceFile, this.Name, this.Path));
#pragma warning restore 4014
            });
		}

		private string defaultPath(string name)
		{
			string path = System.IO.Path.Combine(this.Application.Context.Settings.DefaultProfileFolder, name);
			int i = 0;
			while (System.IO.Directory.Exists(path))
			{
				i++;
				path = System.IO.Path.Combine(this.Application.Context.Settings.DefaultProfileFolder, name + $" ({i})");
			}
			return path;
		}


		public event PropertyChangedEventHandler PropertyChanged;

		protected void OnPropertyChanged(PropertyChangedEventArgs e)
		{
			PropertyChanged?.Invoke(this, e);
		}
	}
}
