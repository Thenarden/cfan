﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using CFAN.Model;
using CFAN.Utils;

namespace CFAN.UI.ViewModel
{
	public class ProfileModListItemViewModel : INotifyPropertyChanged, IComparable<ProfileModListItemViewModel>
	{
		private bool isEnabled;
		public string Name { get; set; }

		public Version Version { get; set; }

		public string Title { get; set; }

		public string Author { get; set; }

		public string Description { get; set; }

		public bool IsEnabled
		{
			get => isEnabled;
			set
			{
				if (this.LinkedMod != null)
					this.LinkedMod.Enabled = value;
				isEnabled = value;
			}
		}

		public bool IsArchive { get; set; }

		public string FileName { get; set; }


		public Mod LinkedMod { get; }

        public ICommand UninstallModCommand { get; }

        public ProfileModListItemViewModel()
		{

		}
		public ProfileModListItemViewModel(Mod mod, SimpleCommand<ProfileModListItemViewModel> uninstallCommand)
		{
			this.LinkedMod = mod;

			this.Name = mod.Name;
			this.Title = mod.Title;
			this.Author = mod.Author;
			this.Description = mod.Description;
			this.FileName = Path.GetFileName(mod.Path);
			this.Version = mod.Version;
			this.IsEnabled = mod.Enabled;
			this.IsArchive = mod.IsArchive;

            this.UninstallModCommand = uninstallCommand;
        }


		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
		{
			PropertyChanged?.Invoke(this, e);
		}

		public int CompareTo(ProfileModListItemViewModel other)
		{
			if (ReferenceEquals(this, other)) return 0;
			if (ReferenceEquals(null, other)) return 1;
			var nameComparison = string.Compare(Name, other.Name, StringComparison.OrdinalIgnoreCase);
			if (nameComparison != 0) return nameComparison;
			return Comparer<Version>.Default.Compare(Version, other.Version);
		}
	}
}
