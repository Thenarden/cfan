﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Input;
using CFAN.Utils;
using Shortbit.Utils;

namespace CFAN.UI.ViewModel
{
	public abstract class ContentViewModel : Disposable
	{
		public App Application => (App)System.Windows.Application.Current;


		public MainWindow Window { get; set; }

		public ICommand ShowViewCommand { get; }
		public ICommand CloseViewCommand { get; }
		public ICommand ShowPopupCommand { get; }
		public ICommand ClosePopupCommand { get; }
		public ICommand CloseActiveCommand { get; }
        
	    public ICommand FocusControlCommand { get; }

		public bool AllowBackButton { get; set; } = true;


		protected ContentViewModel()
		{
			this.ShowViewCommand = new SimpleCommand<ViewContentViewModel>(this.ShowView);
			this.CloseViewCommand = new SimpleCommand(o => this.CloseView());
			this.ShowPopupCommand = new SimpleCommand<PopupContentViewModel>(this.ShowPopup);
			this.ClosePopupCommand = new SimpleCommand(o => this.ClosePopup());
			this.CloseActiveCommand = new SimpleCommand(o => this.CloseActive());
            
		    this.FocusControlCommand = new FocusElementCommand();
		}

		public void ShowView(ViewContentViewModel newView)
		{
			this.ShowView(newView, true);
		}
		public void ShowView(ViewContentViewModel newView, bool appendHistory)
		{
			if (this.Window == null)
				throw new InvalidOperationException("Can't show view without hosting window.");

			this.Window.ShowView(newView, appendHistory);
		}

		public void CloseView()
		{
			if (this.Window == null)
				throw new InvalidOperationException("Can't revert view without hosting window.");

			this.Window.CloseView();
		}

		public void ShowPopup(PopupContentViewModel newView)
		{
			this.ShowPopup(newView, true);
		}
		public void ShowPopup(PopupContentViewModel newView, bool appendHistory)
		{
			if (this.Window == null)
				throw new InvalidOperationException("Can't show popup without hosting window.");

			this.Window.ShowPopup(newView, appendHistory);
		}

		public void ClosePopup()
		{
			if (this.Window == null)
				throw new InvalidOperationException("Can't revert popup without hosting window.");

			this.Window.ClosePopup();
		}

		public void CloseActive()
		{
			if (this.Window == null)
				throw new InvalidOperationException("Can't revert popup without hosting window.");

			this.Window.CloseActive();
		}
	}
}
