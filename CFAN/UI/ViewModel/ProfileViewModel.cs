﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;
using CFAN.Model;
using CFAN.Properties;
using CFAN.Utils;
using Image = System.Drawing.Image;

namespace CFAN.UI.ViewModel
{
    public class ProfileViewModel : ViewContentViewModel, INotifyPropertyChanged
    {

		public Profile RelatedProfile { get; }


		public string Title { get; set; }

		public string Author { get; set; }

	    public string Description { get; set; }

		public Version FactorioVersion { get; set; }

	    public Version ProfileVersion { get; set; }

		public Image Icon { get; set; }

		public Guid Id { get; set; }

	    public Guid LocalId { get; set; }

		public bool InProgress { get; set; }


		public string TxtAuthor => string.IsNullOrWhiteSpace(this.Author) ? null : ("by " + this.Author);
	    public string TxtProfileVersion => (ProfileVersion != null) ? $"ProfileView Version: {ProfileVersion}" : null;
		public string TxtFactorioVersion => (FactorioVersion != null) ? $"Factorio Version: {FactorioVersion}" : null;


		public ObservableCollection<ProfileModListItemViewModel> Items { get; }


		public ICommand PlayCommand { get; }

		public ICommand AdditionalActionsCommand { get; }

		public ICommand InstallModsCommand { get; }

        public SimpleCommand<ProfileModListItemViewModel> UninstallModCommand { get; }

        public ICommand OpenFolderCommand { get; }

	    public ICommand ProfileOptionsCommand { get; }

	    public ICommand ExportProfileCommand { get; }

	    public ICommand DeleteProfileCommand { get; }



	    public ProfileViewModel(Profile profile)
	    {
		    this.RelatedProfile = profile;

		    this.Title = profile.Name;
		    this.Author = profile.Author;
		    this.Description = profile.Description;
		    this.Icon = profile.Icon ?? Resources.FactorioGear;

		    this.ProfileVersion = profile.ProfileVersion;
		    this.FactorioVersion = profile.GameVersion;
		    this.Id = profile.Id;
		    this.LocalId = profile.LocalId;


		    this.RelatedProfile.PropertyChanged += this.ProfileOnPropertyChanged;

			this.PlayCommand = new SimpleCommand(o => this.RelatedProfile.Launch());

			this.AdditionalActionsCommand = new SimpleCommand<Button>(a =>
			{
				a.ContextMenu.PlacementTarget = a;
				a.ContextMenu.IsOpen = !a.ContextMenu.IsOpen;
			});

			this.InstallModsCommand = new SimpleCommand(o => this.ShowView(new ModListViewModel(this.RelatedProfile)));
            this.UninstallModCommand = new SimpleCommand<ProfileModListItemViewModel>(
                vm => this.Window.Application.Context.Mods.UninstallMod(this.RelatedProfile, vm.LinkedMod.Name));

			this.OpenFolderCommand = new SimpleCommand(o =>
			{
				Process.Start(this.RelatedProfile.ProfileFolder);
			});

			this.ExportProfileCommand = new SimpleCommand(o =>
			{
				this.ShowPopup(new ExportProfileViewModel(this.RelatedProfile));
			});

			this.DeleteProfileCommand = new SimpleCommand(o =>
			{
				this.ShowPopup(new DeleteProfileViewModel(this.RelatedProfile, true));
			});

            profile.Mods.CollectionChanged += ProfileMods_OnCollectionChanged;
            var items = profile.Mods.Mods.Select(m => new ProfileModListItemViewModel(m, UninstallModCommand)).ToList();
            items.Sort();
            this.Items = new ObservableCollection<ProfileModListItemViewModel>(items);
        }

        private void ProfileMods_OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs args)
        {
            if (!this.Window.Dispatcher.CheckAccess())
            {
                this.Window.Dispatcher.Invoke(() => this.ProfileMods_OnCollectionChanged(sender, args));
                return;
            }

            if (args.Action == NotifyCollectionChangedAction.Reset)
            {

                var items = this.RelatedProfile.Mods.Mods.Select(m => new ProfileModListItemViewModel(m, UninstallModCommand)).ToList();
                items.Sort();
                var read = ((ICollection<ProfileModListItemViewModel>) this.Items).IsReadOnly;
                this.Items.Clear();
                foreach (var vm in items)
                {
                    this.Items.Add(vm);
                }
            }

            if (args.Action == NotifyCollectionChangedAction.Remove ||
                args.Action == NotifyCollectionChangedAction.Replace)
            {
                foreach (KeyValuePair<string, Mod> mod in args.OldItems)
                {
                    var vm = this.Items.FirstOrDefault(v => v.LinkedMod == mod.Value);
                    if (vm != null)
                        this.Items.Remove(vm);
                }
            }
            if (args.Action == NotifyCollectionChangedAction.Add ||
                args.Action == NotifyCollectionChangedAction.Replace)
            {
                foreach (KeyValuePair<string,Mod> mod in args.NewItems)
                {
                    var vm = new ProfileModListItemViewModel(mod.Value, UninstallModCommand);
                    bool added = false;
                    for (int i = 0; i < this.Items.Count; i++)
                    {
                        if (vm.CompareTo(this.Items[i]) < 0)
                        {
                            this.Items.Insert(i, vm);
                            added = true;
                            break;
                        }
                    }
                    if (!added)
                        this.Items.Add(vm);
                }
            }
        }


        protected override void DisposeManaged()
	    {
		    base.DisposeManaged();
		    if (this.RelatedProfile != null)
			    this.RelatedProfile.PropertyChanged -= this.ProfileOnPropertyChanged;
		}


	    private void ProfileOnPropertyChanged(object sender, PropertyChangedEventArgs args)
	    {
		    switch (args.PropertyName)
		    {
			    case nameof(Profile.Icon):
				    this.Icon = this.RelatedProfile.Icon ?? Resources.FactorioGear;
				    break;
			    case nameof(Profile.Name):
				    this.Title = this.RelatedProfile.Name;
				    break;
			    case nameof(Profile.GameVersion):
				    this.FactorioVersion = this.RelatedProfile.GameVersion;
				    break;
			    case nameof(Profile.Id):
				    this.Id = this.RelatedProfile.Id;
				    break;
			    case nameof(Profile.LocalId):
				    this.LocalId = this.RelatedProfile.LocalId;
				    break;
				case nameof(Profile.InProgress):
				    this.InProgress = this.RelatedProfile.InProgress;
				    break;

		    }
	    }


		public event PropertyChangedEventHandler PropertyChanged;

	    protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
	    {
		    PropertyChanged?.Invoke(this, e);
	    }
	}
}
