﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Markup;
using CFAN.Factorio.Api;
using CFAN.Model;
using CFAN.Utils;
using MoreLinq;
using Shortbit.Utils;

namespace CFAN.UI.ViewModel
{
	public class ModListViewModel : ViewContentViewModel, INotifyPropertyChanged
	{
	    private const int UserInputWaitTime = 500;

		private string query;
		public string Query
		{
			get => this.query;
			set
			{
				this.query = value;

				this.inputUpdateTimer.Change(UserInputWaitTime, Timeout.Infinite);
			}
		}

		private string author;
		public string Author
		{
			get => this.author;
			set
			{
				this.author = value;
                
				this.inputUpdateTimer.Change(UserInputWaitTime, Timeout.Infinite);
			}
		}

		private ModListOrderViewModel order;
		public ModListOrderViewModel Order
		{
			get => this.order;
			set
			{
				this.order = value;
                
				this.ReloadModList();
			}
		}

	    public ObservableCollection<ModListOrderViewModel> AvailableModListOrders { get; set; }


	    private bool searchAllVersions;
        public bool SearchAllVersions
        {
            get => this.searchAllVersions;
            set
            {
                this.searchAllVersions = value;
                
                this.ReloadModList();
            }
        }
        
	    public bool LoadMoreCommandEnabled => this.MoreItemsAvailable && !this.UpdateRunning;
        
	    [DependsOn(nameof(RelatedProfile)), DependsOn(nameof(SearchAllVersions))]
	    public string TxtVersionFilterButtonTooltip => this.RelatedProfile == null || this.SearchAllVersions
	                                                    ? "Allow all Factorio versions"
	                                                    : $"Limited to ProfileView Version ({this.RelatedProfile.MainGameVersion})";
        
	    [DependsOn(nameof(MoreItemsAvailable)), DependsOn(nameof(UpdateRunning))]
	    public string TxtLoadMoreLabel
	    {
	        get
	        {
	            if (!this.MoreItemsAvailable)
	                return "No more results found";
	            if (this.UpdateRunning)
	                return "Already loading more mods";
	            return "Load more mods";
	        }
	    }
	    public bool UpdateRunning { get; private set; } // Can't use the reset event since it does not trigger the PropertyChanged
        public bool MoreItemsAvailable { get; private set; }



		public ObservableCollection<ModListItemViewModel> FoundMods { get; } = new ObservableCollection<ModListItemViewModel>();


		public Profile RelatedProfile { get; }


		public ICommand RefreshCommand { get; }
        
	    public SimpleCommand<string> AuthorSearchCommand { get; }
		public SimpleCommand<ModListItemViewModel> InstallCommand { get; }
        public ICommand ToggleVersionFilter { get; }
		public ICommand RequestMoreItemsCommand { get; }

	    private IEnumerator<ModInfoFull> results;
        private readonly Timer inputUpdateTimer;
	    private readonly ManualResetEvent updateTaskFinished;
	    private CancellationTokenSource cancellationTokenSource;

		public ModListViewModel()
			: this(null, null, null)
		{
		}

	    public ModListViewModel(string initialQuery, string initialAuthor)
	        : this(null, initialQuery, initialAuthor)
	    {
	    }

	    public ModListViewModel(Profile profile)
	        : this(profile, null, null)
	    {}

		public ModListViewModel(Profile profile, string initialQuery, string initialAuthor)
		{
			this.RelatedProfile = profile;
		    this.author = initialAuthor;
		    this.query = initialQuery;

            if (this.RelatedProfile != null)
                this.RelatedProfile.Mods.CollectionChanged += ProfileMods_OnCollectionChanged;
            
            this.AvailableModListOrders = new ObservableCollection<ModListOrderViewModel>(EnumEx.GetValues<ModListOrder>().Select(mlo => new ModListOrderViewModel(mlo)));
		    this.Order = AvailableModListOrders.First(mlo => mlo.Order == ModListOrder.RecentlyUpdated);

			this.inputUpdateTimer = new Timer(this.OnTimer, null, Timeout.Infinite, Timeout.Infinite);
           

            this.ToggleVersionFilter = new SimpleCommand(o => { this.SearchAllVersions = !this.SearchAllVersions; });
            this.AuthorSearchCommand = new SimpleCommand<string>(owner => this.ShowView(new ModListViewModel(this.RelatedProfile, null, owner)));

			this.updateTaskFinished = new ManualResetEvent(true);
			this.RequestMoreItemsCommand = new SimpleCommand(o =>
			{
				if (this.results == null || !this.updateTaskFinished.WaitOne(0))
					return;

                this.AddNewItems(10, false);
			});
		    this.RefreshCommand = new SimpleCommand(this.ReloadModList);

            this.InstallCommand = new SimpleCommand<ModListItemViewModel>(
                async vm => {
                    if (vm.Mod.IsCompatible(this.RelatedProfile.MainGameVersion))
                        await this.Window.Application.Context.Mods
                            .InstallMod(
                                this.RelatedProfile, vm.Mod,
                                vm.Mod.LatestMatchingRelease(
                                    this.RelatedProfile
                                        .MainGameVersion)
                                .Version);
                    else
                        await this.Window.Application.Context.Mods
                                .InstallMod(
                                    this.RelatedProfile, vm.Mod,
                                    vm.Mod.Releases.MaxBy(r => r.Version).Version);
                });

		    this.inputUpdateTimer.Change(10, Timeout.Infinite);
		}

        private void ProfileMods_OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
        {
            foreach (var vm in this.FoundMods)
            {
                vm.IsInstalled = (this.RelatedProfile.Mods.Contains(vm.Mod.Name));
            }
        }


        private void OnTimer(object args)
	    {
	        this.ReloadModList();
	    }
        
	    public void ReloadModList()
	    {
	        this.AddNewItems(25, true);
	    }
	    private void AddNewItems(int amount, bool clear)
	    {
	        if (this.Window == null)
	            return;

	        if (this.Window.Dispatcher.CheckAccess())
	        {
	            Task.Run(() => this.AddNewItems(amount, clear));
	            return;
	        }

	        this.cancellationTokenSource?.Cancel();

	        if (!this.updateTaskFinished.WaitOne(5000))
	        {
	            Debug.WriteLine("ModListView cancelation timed out.");
	            return;
	        }
	        this.updateTaskFinished.Reset();
	        this.UpdateRunning = true;
	        this.cancellationTokenSource = new CancellationTokenSource();
	        var token = this.cancellationTokenSource.Token;

	        try
	        {

	            if (clear || this.results == null)
	            {
	                this.Window.Dispatcher.Invoke(this.FoundMods.Clear);
                    
	                var enumerable =  this.Application.Context.ModInfoAdapter.GetExtendedModList(
	                    query: this.Query,
	                    owner: this.Author,
	                    factorioVersion: this.searchAllVersions || this.RelatedProfile == null
	                                         ? null
	                                         : this.RelatedProfile.MainGameVersion,
	                    order: this.Order.Order);
	                this.results = enumerable.GetEnumerator();

	                token.ThrowIfCancellationRequested();
	            }

	            for (int i = 0; i < amount; i++)
	            {
	                this.MoreItemsAvailable = this.results.MoveNext();
	                if (!this.MoreItemsAvailable)
	                    break;
	                var mi = this.results.Current;
	                Debug.Assert(mi != null, nameof(mi) + " != null");

	                token.ThrowIfCancellationRequested();
	                this.Window.Dispatcher.Invoke(() =>
	                {
	                    this.FoundMods.Add(new ModListItemViewModel(mi, 
	                                                                RelatedProfile?.Mods.Contains(mi.Name) ?? false,
                                                                    mi.IsCompatible(RelatedProfile?.MainGameVersion),
	                                                                this.InstallCommand, this.AuthorSearchCommand));
	                });
	            }
	        }
	        catch (OperationCanceledException)
	        {}

	        this.cancellationTokenSource?.Dispose();
	        this.cancellationTokenSource = null;
	        this.UpdateRunning = false;
	        this.updateTaskFinished.Set();
	    }


		public event PropertyChangedEventHandler PropertyChanged;

		protected void OnPropertyChanged(PropertyChangedEventArgs e)
		{
			PropertyChanged?.Invoke(this, e);
		}
	}
}
