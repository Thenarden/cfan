﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using CFAN.Tasks;
using MoreLinq;

namespace CFAN.UI.ViewModel
{
    public class CompositeTaskViewModel : BackgroundTaskViewModel
    {

        public CompositeTask RelatedCompositeTask { get; }

        public ObservableCollection<BackgroundTaskViewModel> SubTaskViewModels { get; }



        public CompositeTaskViewModel(CompositeTask relatedCompositeTask, Dispatcher dispatcher)
        : base(relatedCompositeTask, dispatcher)
        {
            this.RelatedCompositeTask = relatedCompositeTask;
            this.RelatedCompositeTask.SubTasks.CollectionChanged += this.SubTasks_OnCollectionChanged;
            
            foreach (var subTask in this.RelatedCompositeTask.SubTasks)
            {
                subTask.Task.PropertyChanged += SubTask_OnPropertyChanged;
            }
            this.SubTaskViewModels = new ObservableCollection<BackgroundTaskViewModel>(
                this.RelatedCompositeTask.SubTasks.Select(CreateViewModel));
        }

        private void SubTask_OnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
        }

        private void UpdateSubTaskVisibility(BackgroundTask task)
        {
            try
            {
                var pair = this.RelatedCompositeTask.SubTasks.Index().First(p => p.Value.Task == task);
            }
            catch (InvalidOperationException)
            {
                return;
            }
        }

        private BackgroundTaskViewModel CreateViewModel(CompositeTask.SubTask task)
        {
            return CreateViewModel(task.Task);
        }
        private BackgroundTaskViewModel CreateViewModel(BackgroundTask task)
        {
            var vm = task is CompositeTask ct
                ? new CompositeTaskViewModel(ct, this.dispatcher)
                : new BackgroundTaskViewModel(task, this.dispatcher);
            //task.PropertyChanged += SubTask_OnPropertyChanged;
            return vm;
        }

        private void SubTasks_OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs args)
        {
            if (!this.dispatcher.CheckAccess())
            {
                this.dispatcher.InvokeAsync(() => this.SubTasks_OnCollectionChanged(sender, args));
                return;
            }

            if (args.Action == NotifyCollectionChangedAction.Reset)
            {
                this.SubTaskViewModels.Clear();
                foreach (var subTask in this.RelatedCompositeTask.SubTasks)
                {
                    this.SubTaskViewModels.Add(CreateViewModel(subTask));
                }

            }

            if (args.Action == NotifyCollectionChangedAction.Remove ||
                args.Action == NotifyCollectionChangedAction.Replace)
            {
                foreach (CompositeTask.SubTask subTask in args.OldItems)
                {
                    //subTask.Task.PropertyChanged -= SubTask_OnPropertyChanged;
                    var vm = this.SubTaskViewModels.First(v => v.RelatedTask == subTask.Task);
                    this.SubTaskViewModels.Remove(vm);
                }
            }

            if (args.Action == NotifyCollectionChangedAction.Add ||
                args.Action == NotifyCollectionChangedAction.Replace)
            {
                var index = args.NewStartingIndex;
                foreach (CompositeTask.SubTask subTask in args.NewItems)
                {
                    this.SubTaskViewModels.Insert(index, CreateViewModel(subTask));
                    index++;
                }
            }
        }
    }
}
