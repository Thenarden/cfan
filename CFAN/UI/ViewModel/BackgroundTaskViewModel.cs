﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using CFAN.Tasks;

namespace CFAN.UI.ViewModel
{
    public class BackgroundTaskViewModel : INotifyPropertyChanged
    {
        protected readonly Dispatcher dispatcher;

        public BackgroundTaskViewModel(BackgroundTask relatedTask, Dispatcher dispatcher)
        {
            this.dispatcher = dispatcher;

            this.RelatedTask = relatedTask;
            this.RelatedTask.PropertyChanged += RelatedTaskOnPropertyChanged;

            this.Description = this.RelatedTask.Description;
            this.CompletedSteps = this.RelatedTask.CompletedSteps;
            this.NumSteps = this.RelatedTask.NumSteps;
            this.NumProgress = (int)(this.RelatedTask.Progress * 100);

        }

        private void RelatedTaskOnPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            if (args.PropertyName == nameof(BackgroundTask.Description))
                this.Description = this.RelatedTask.Description;
            if (args.PropertyName == nameof(BackgroundTask.CompletedSteps))
            {
                this.CompletedSteps = this.RelatedTask.CompletedSteps;
                this.NumProgress = (int)(this.RelatedTask.Progress * 100);
            }
            if (args.PropertyName == nameof(BackgroundTask.NumSteps))
            {
                this.NumSteps = this.RelatedTask.NumSteps;
                this.NumProgress = (int)(this.RelatedTask.Progress * 100);
            }
            if (args.PropertyName == nameof(BackgroundTask.State))
            {
                this.IsVisibile = this.RelatedTask.State != BackgroundTaskState.Waiting;
                this.State = this.RelatedTask.State;
            }

        }

        public BackgroundTask RelatedTask { get; }

        public BackgroundTaskState State { get; private set; }
        public bool IsFailed { get; private set; }
        public bool IsCanceled { get; private set; }
        public bool IsRunning { get; private set; }
        public bool IsVisibile { get; private set; }

        public Visibility SelfVisibility => this.IsVisibile ? Visibility.Visible : Visibility.Collapsed;
        
        public Visibility ProgressVisibility => this.HasProgress ? Visibility.Visible : Visibility.Collapsed;

        public bool HasProgress => this.NumSteps > 1;

        public string Description { get; private set; }
        public int CompletedSteps { get; private set; }
        public int NumSteps { get; private set; }
        public int NumProgress { get; private set; }


        public string TxtSteps => $"completed {this.CompletedSteps} of {this.NumSteps}";


        public event PropertyChangedEventHandler PropertyChanged;
    }
}
