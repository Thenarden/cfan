﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using CFAN.Model;
using CFAN.Utils;

namespace CFAN.UI.ViewModel
{
	public class DeleteProfileViewModel : PopupContentViewModel
	{

		public string Title { get; set; }

		public ICommand ConfirmCommand { get; set; }


		public DeleteProfileViewModel(Profile profile, bool closeView)
		{
			this.Title = profile.Name;
			this.ConfirmCommand = new SimpleCommand(o =>
			{
				this.ClosePopup();
				if (closeView)
					this.CloseView();

				this.Application.Context.Profile.Delete(profile);
			});
		}

	}
}
