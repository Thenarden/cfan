﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CFAN.Factorio.Api;

namespace CFAN.UI.ViewModel
{
    public class ModListOrderViewModel
    {
        public ModListOrder Order { get; }


        public ModListOrderViewModel(ModListOrder order)
        {
            this.Order = order;
        }

        public override string ToString()
        {
            return ModListOrderInfo.DisplayText[this.Order];
        }
    }
}
