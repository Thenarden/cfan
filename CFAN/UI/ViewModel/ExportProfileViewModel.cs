﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using CFAN.Model;
using CFAN.Utils;
using PropertyChanged;

namespace CFAN.UI.ViewModel
{
	public class ExportProfileViewModel : PopupContentViewModel, INotifyPropertyChanged
	{
		public Profile RelatedProfile { get; }

		public string DefaultFolder
		{
			get
			{
				if (!string.IsNullOrWhiteSpace(this.Application.Context.Settings.LastExportFolder))
					return this.Application.Context.Settings.LastExportFolder;
				return Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
			}
		}


		private string name;
		public string Name
		{
			get => name;
			set
			{
				if (!this.pathChanged)
				{
					this.path = System.IO.Path.Combine(DefaultFolder, value.Trim() + ".zip");
				}
				name = value;
			}
		}


		private string path;
		[DependsOn(nameof(Name))]
		public string Path
		{
			get => path;
			set
			{
				path = value;
				this.pathChanged = true;
			}
		}


		public bool Mimimalistic { get; set; }

		public ICommand ConfirmCommand { get; }
		

		private bool pathChanged = false;

		public ExportProfileViewModel(Profile profile)
		{
			this.RelatedProfile = profile;
			this.Name = profile.Name;

			this.ConfirmCommand = new SimpleCommand(o =>
			{
				if (string.IsNullOrWhiteSpace(this.Name) || string.IsNullOrWhiteSpace(this.Path))
					return;

				this.Application.Context.Settings.LastExportFolder = System.IO.Path.GetDirectoryName(this.Path);
				this.Application.Context.Profile.Export(this.RelatedProfile, this.Path, this.Mimimalistic, this.Name);
				this.ClosePopup();
			});
		}


		public event PropertyChangedEventHandler PropertyChanged;

		protected void OnPropertyChanged(PropertyChangedEventArgs e)
		{
			PropertyChanged?.Invoke(this, e);
		}
	}
}
