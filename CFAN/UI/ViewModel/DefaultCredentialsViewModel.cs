﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using CFAN.Model;
using CFAN.Utils;

namespace CFAN.UI.ViewModel
{
	public class DefaultCredentialsViewModel : PopupContentViewModel
	{
		public string Username { get; set; }


		public SecureString Password { get; set; }


		public ICommand ConfirmCommand { get; }

		public DefaultCredentialsViewModel()
		{
			this.AllowOutOfBorderClick = false;
			this.AllowBackButton = false;

			this.ConfirmCommand = new SimpleCommand(o =>
			{
				this.Application.Context.Credentials.Add(this.Application.Context.Settings.DefaultCredentials,
					new Credentials(this.Username, this.Password));
				this.Password.Dispose();
				this.ClosePopup();
			});
		}
	}
}
