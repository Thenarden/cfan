﻿using System;
using System.ComponentModel;
using System.Drawing;
using CFAN.Model;
using CFAN.Properties;
using CFAN.Utils;
using Shortbit.Utils;

namespace CFAN.UI.ViewModel
{
	public class ProfileListItemViewModel : Disposable,  INotifyPropertyChanged
	{


		public Image Icon { get; set; } = Resources.FactorioGear;

		private Image hoverIcon = null;
		public Image HoverIcon
		{
			get => this.hoverIcon ?? this.Icon;
			set => hoverIcon = value;
		}

		public string Title { get; set; }

		public Guid? Id { get; set; }

		public Guid? LocalId { get; set; }

		public Version FactorioVersion { get; set; }

		public bool InProgress { get; set; }

		public SimpleCommand<ProfileListItemViewModel> LeftClick { get; set; }
		public SimpleCommand<ProfileListItemViewModel> RightClick { get; set; }


		public string IdText
		{
			get
			{
				if (!this.Id.HasValue)
					return "";
				var txt = this.Id.Value.ToString("D");

				int idx = txt.IndexOf("-", StringComparison.Ordinal);
				idx = txt.IndexOf("-", idx+1, StringComparison.Ordinal);

				return "{" + txt.Substring(0, idx) + "}";

			}
		}
		public string FactorioVersionText => FactorioVersion != null ? $"Version: {FactorioVersion}" : "";


		public Profile RelatedProfile { get; }

		public ProfileListItemViewModel()
		{

		}

		public ProfileListItemViewModel(Profile profile)
		{
			this.RelatedProfile = profile;

			this.Icon = profile.Icon ?? Resources.FactorioGear;
			this.Title = profile.Name;
			this.FactorioVersion = profile.GameVersion;
			this.Id = profile.Id;
			this.LocalId = profile.LocalId;
			this.InProgress = profile.InProgress;

			this.RelatedProfile.PropertyChanged += this.ProfileOnPropertyChanged;
		}

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
		{
			PropertyChanged?.Invoke(this, e);
		}


		private void ProfileOnPropertyChanged(object sender, PropertyChangedEventArgs args)
		{
			switch (args.PropertyName)
			{
				case nameof(Profile.Icon):
					this.Icon = this.RelatedProfile.Icon ?? Resources.FactorioGear;
					break;
				case nameof(Profile.Name):
					this.Title = this.RelatedProfile.Name;
					break;
				case nameof(Profile.GameVersion):
					this.FactorioVersion = this.RelatedProfile.GameVersion;
					break;
				case nameof(Profile.Id):
					this.Id = this.RelatedProfile.Id;
					break;
				case nameof(Profile.LocalId):
					this.LocalId = this.RelatedProfile.LocalId;
					break;
				case nameof(Profile.InProgress):
					this.InProgress = this.RelatedProfile.InProgress;
					break;

			}
		}

		protected override void DisposeManaged()
		{
			base.DisposeManaged();
			if (this.RelatedProfile != null)
				this.RelatedProfile.PropertyChanged -= this.ProfileOnPropertyChanged;
		}
	}
}
