﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Shortbit.Utils;
using Shortbit.Utils.Collections.Generic;

namespace CFAN.Model
{
	public class ModList : IEnumerable<KeyValuePair<string, Mod>>, IEnumerable, INotifyCollectionChanged, INotifyPropertyChanged
	{


		private readonly ObservableDictionary<string, Mod> mods;

		public ModList()
		{
			this.mods = new ObservableDictionary<string, Mod>();
		}
		public ModList(IDictionary<string, Mod> exising)
		{
			this.mods = new ObservableDictionary<string, Mod>(exising);
			foreach (var mod in exising.Values)
			{
				mod.PropertyChanged += ModOnPropertyChanged;
			}
		}

		public void SaveEnabledMod(string folder)
		{
			var enabledMods = this.mods.Values.ToDictionary(m => m.Name, m => m.Enabled);
			
			ModListStore.Save(folder, enabledMods);
		}

		public static ModList LoadFromFolder(string folder, bool defaultActive)
		{
			if (!Directory.Exists(folder))
				return new ModList();

			var dir = new DirectoryInfo(folder);

			var enabledMods = ModListStore.Load(folder);
			var mods = new List<Mod>();

			foreach (var info in (dir.EnumerateFiles("*.zip") as IEnumerable<FileSystemInfo>).Concat(dir.EnumerateDirectories()))
			{
				if (PathEx.IsDirectory(info.FullName) && !File.Exists(Path.Combine(info.FullName, "info.json")))
					continue;
				
				var mod = Mod.FromPath(info.FullName, enabledMods, defaultActive);
				mods.Add(mod);
			}

			return new ModList(mods.ToDictionary(m => m.Name, m => m));

		}

		public void Reload(string folder, bool defaultActive)
		{
			if (!Directory.Exists(folder))
				return;

			var dir = new DirectoryInfo(folder);

			var enabledMods = ModListStore.Load(folder);
			var mods = new List<Mod>();

			foreach (var info in (dir.EnumerateFiles("*.zip") as IEnumerable<FileSystemInfo>).Concat(dir.EnumerateDirectories()))
			{
				if (PathEx.IsDirectory(info.FullName) && !File.Exists(Path.Combine(info.FullName, "info.json")))
					continue;

				var mod = Mod.FromPath(info.FullName, enabledMods, defaultActive);
				mods.Add(mod);
			}

            try
            {
                this.mods.Clear();
                this.mods.AddRange(mods.ToDictionary(m => m.Name, m => m));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
		}


		public int Count => this.mods.Count;
		public Mod this[string key]
		{
			get => this.mods[key];
			//set => this.mods[key] = value;
		}


		public ICollection<Mod> Values => this.mods.Values;
		public ICollection<Mod> Mods => this.Values;

		public bool Contains(string modName)
		{
			return this.mods.ContainsKey(modName);
		}

		public bool Contains(Mod mod)
		{
			return this.Contains(mod.Name);
		}
        
	   /* public bool Remove(Mod mod)
	    {
	        return this.Remove(mod.Name);
	    }
	    public bool Remove(string modName)
	    {
	        return this.mods.Remove(modName);
	    }*/


		public event NotifyCollectionChangedEventHandler CollectionChanged
		{
			add => this.mods.CollectionChanged += value;
			remove => this.mods.CollectionChanged -= value;
		}
		public event PropertyChangedEventHandler PropertyChanged
		{
			add => this.mods.PropertyChanged += value;
			remove => this.mods.PropertyChanged -= value;
		}

		public IEnumerator<KeyValuePair<string, Mod>> GetEnumerator() => this.mods.GetEnumerator();

		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

		public event PropertyChangedEventHandler ModPropertyChanged;

		private void ModOnPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			ModPropertyChanged?.Invoke(sender, e);
		}

#pragma warning disable IDE1006 // Naming Styles
		private class ModListStoreEntry
		{
			public string name { get; set; }
			public bool enabled { get; set; }
		}
		private class ModListStore
		{
			// ReSharper disable once CollectionNeverUpdated.Local
			public List<ModListStoreEntry> mods { get; set; }

			public static Dictionary<string, bool> Load(string folder)
			{
				var enabledMods = new Dictionary<string, bool>();
				if (File.Exists(Path.Combine(folder, "mod-list.json")))
				{
					var mod_list = JsonConvert.DeserializeObject<ModListStore>(File.ReadAllText(Path.Combine(folder, "mod-list.json")));
					foreach (var entry in mod_list.mods)
					{
						enabledMods[entry.name] = entry.enabled;
					}
				}
				return enabledMods;
			}

			public static void Save(string folder, Dictionary<string, bool> enabledMods)
			{
				enabledMods["base"] = true;
				var store = new ModListStore
				{
					mods = enabledMods.Select(p => new ModListStoreEntry { name=p.Key, enabled = p.Value}).ToList()
				};
				File.WriteAllText(Path.Combine(folder, "mod-list.json"), JsonConvert.SerializeObject(store, Formatting.Indented));
			}
		}
#pragma warning restore IDE1006 // Naming Styles
	}
}
