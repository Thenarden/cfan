﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using CFAN.Factorio.Api;
using Newtonsoft.Json;
using PropertyChanged;
using Shortbit.Utils.Properties;

namespace CFAN.Model
{
	public class Profile : INotifyPropertyChanged
	{


		public Profile(string name, string profileFolder, Guid id = default(Guid), Guid localId = default(Guid))
		{
			this.Name = name;
			this.Name = name;
			this.ProfileFolder = profileFolder;
			this.Id = id == Guid.Empty ? Guid.NewGuid() : id;
			this.LocalId = localId == Guid.Empty ? Guid.NewGuid() : localId;


			this.Config = GameConfig.LoadOrGenerate(this.ProfileFolder);
			if (this.Config.NeedsFix())
				GameConfig.FixPathes(this.ProfileFolder);


			this.Mods = ModList.LoadFromFolder(this.ModsFolder, this.Config.EnableNewMods);
			this.Mods.ModPropertyChanged += ModsOnModPropertyChanged;
		}

		private void ModsOnModPropertyChanged(object sender, PropertyChangedEventArgs args)
		{
			if (args.PropertyName == nameof(Mod.Enabled))
				this.Mods.SaveEnabledMod(this.ModsFolder);
		}

		[JsonIgnore]
		[DoNotNotify]
		public GameConfig Config { get; }

		[JsonIgnore]
		[DoNotNotify]
		public ModList Mods { get; }

		public Guid Id { get; }

		public Guid LocalId { get; }

		public string Name { get; set; }

		public string Author { get; set; }

		public string Description { get; set; }

		[NotNull]
		public string ProfileFolder { get; set; }

	    [JsonIgnore]
        [DependsOn(nameof(ProfileFolder))]
	    public string ModsFolder => Path.Combine(this.ProfileFolder, "mods");


		public string IconFile { get; set; } = "Icon.png";

		[JsonIgnore]
		public string FullIconFile => Path.IsPathRooted(IconFile) ? this.IconFile : Path.Combine(this.ProfileFolder, this.IconFile);

		
		public bool InProgress { get; set; }

		[JsonIgnore] 
		[DependsOn("IconFile")]
		public Image Icon
		{
			get
			{
				if (!File.Exists(this.FullIconFile))
					return null;
				var img = Image.FromFile(this.FullIconFile);
				var res = new Bitmap(img);
				img.Dispose();
				return res;
			}
			set
			{
				if (string.IsNullOrWhiteSpace(this.IconFile))
					this.IconFile = "Icon.png";
				value.Save(this.FullIconFile);
			}
		}

		public Version ProfileVersion { get; set; }

		public Version GameVersion { get; set; }
		
		[JsonIgnore]
		public Version MainGameVersion => new Version(this.GameVersion.Major, this.GameVersion.Minor);


		public void ReloadMods()
		{
			this.Mods.Reload(this.ModsFolder, this.Config.EnableNewMods);
		}

		public void Launch()
		{
			var file = Path.Combine(this.ProfileFolder, @"bin\x64\factorio.exe");
			Process.Start(file);
		}

		
		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
		{
			PropertyChanged?.Invoke(this, e);
		}
	}
}
