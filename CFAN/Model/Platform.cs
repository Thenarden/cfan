﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CFAN.Model
{
	public enum Platform
	{
		Win32,
		Win64,
		Osx,
		Linux64,
		HeadlessLinux64,
	}

	public static class PlatformInfo
	{

		public static readonly IReadOnlyDictionary<Platform, string> DisplayText = new ReadOnlyDictionary<Platform, string>(
			new Dictionary<Platform, string>
			{
				{ Platform.Win32, "Windows (32 bit)" },
				{ Platform.Win64, "Windows (64 bit)" },
				{ Platform.Osx, "OSX" },
				{ Platform.Linux64, "Linux (64bit)" },
				{ Platform.HeadlessLinux64, "Headless Server (Linux, 64bit)" },
			});
		public static readonly IReadOnlyDictionary<Platform, string> Extension = new ReadOnlyDictionary<Platform, string>(
			new Dictionary<Platform, string>
			{
				{ Platform.Win32, ".zip" },
				{ Platform.Win64, ".zip" },
				{ Platform.Osx, ".dmg" },
				{ Platform.Linux64, ".tar.xz" },
				{ Platform.HeadlessLinux64, ".tar.xz" },
			});
		
		public static Platform CurrentPlatform
		{
			get
			{
				switch (Environment.OSVersion.Platform)
				{
					case PlatformID.Unix:
						return Platform.Linux64;
					case PlatformID.MacOSX:
						return Platform.Osx;

					case PlatformID.Win32NT:
						if (Environment.Is64BitOperatingSystem)
							return Platform.Win64;
						else
							return Platform.Win32;

					case PlatformID.Win32S:
					case PlatformID.Win32Windows:
					case PlatformID.WinCE:
					default:
						throw new NotSupportedException();
				}
			}
		}
	}
}
