﻿using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Cryptography;
using Shortbit.Utils;

namespace CFAN.Model
{
	public class Credentials : INotifyPropertyChanged
	{
		public byte[] Entropy { get; }

		public byte[] EncryptedPassword { get; private set; }

		private string username;

		public string Username
		{
			get => username;
			set => username = value;
		}


		public SecureString Password
		{
			get => this.UnprotectPassword(this.EncryptedPassword);
			set => this.EncryptedPassword = this.ProtectPassword(value);
		}


		public Credentials(string username, char[] password)
			: this(username, GenerateEntropy(), password)
		{}
		public Credentials(string username, SecureString password)
			: this(username, GenerateEntropy(), password)
		{}
		public Credentials(string username, byte[] entropy, char[] password)
			: this(username, entropy, SecureStringFromChars(password))
		{
		}
		public Credentials(string username, byte[] entropy, SecureString password)
			: this(username, entropy)
		{
			this.EncryptedPassword = this.ProtectPassword(password);
		}

		public Credentials(string username, byte[] entropy, byte[] encryptedPassword)
			: this(username, entropy)
		{
			this.EncryptedPassword = encryptedPassword;
		}

		private Credentials(string username, byte[] entropy)
		{
			this.username = username;
			this.Entropy = entropy;
		}






		public static byte[] GenerateEntropy()
		{
			var entropy = new byte[20];
			var rng = new RNGCryptoServiceProvider();
			rng.GetBytes(entropy);
			return entropy;
		}

		public static SecureString SecureStringFromChars(char[] password)
		{
			var ss = new SecureString();
			for (int i = 0; i < password.Length; i++)
			{
				ss.AppendChar(password[i]);
				password[i] = '\0';
			}
			return ss;
		}

		private byte[] ProtectPassword(SecureString password)
		{
			return password.Process(bts => ProtectedData.Protect(bts, this.Entropy, DataProtectionScope.CurrentUser));
		}

		private SecureString UnprotectPassword(byte[] encryptedPassword)
		{
			byte[] unprotectedData = ProtectedData.Unprotect(encryptedPassword, this.Entropy, DataProtectionScope.CurrentUser);
			var bytesPinHandle = GCHandle.Alloc(unprotectedData, GCHandleType.Pinned);

			var ss = new SecureString();
			ss.Load((ref byte[] data) =>
				{
					for (int i = 0; i < unprotectedData.Length; i++)
					{
						data[i] = unprotectedData[i];
						unprotectedData[i] = 0;
					}
					bytesPinHandle.Free();
				}, unprotectedData.Length);

			ss.MakeReadOnly();
			return ss;
		}


		public event PropertyChangedEventHandler PropertyChanged;


		protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
		{
			PropertyChanged?.Invoke(this, e);
		}
	}
}
