﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text.RegularExpressions;
using CFAN.Exceptions;
using CFAN.Factorio.Api;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Shortbit.Utils;

namespace CFAN.Model
{
	public class Mod : INotifyPropertyChanged, IComparable<Mod>
	{
		public static readonly Regex NameRegex = new Regex(@"^(?<name>[\w_\-\.\s]+)_(?<version>\d+\.\d+(\.\d+(\.\d+)?)?)(.zip)?$");
		public static readonly Regex InfoJsonRegex = new Regex(@"^[\w_\-\.\s]+/info\.json$");



		public string Name { get; set; }

		public string Author { get; set; }

		public string Title { get; set; }

		public string Description { get; set; }

		public Version Version { get; set; }
		
		public bool Enabled { get; set; }
		
		public string Path { get; set; }
		
		public bool IsArchive => !string.IsNullOrWhiteSpace(this.Path) && System.IO.File.Exists(this.Path);


		public static Mod FromPath(string path, Dictionary<string, bool> enabledList, bool defaultOn)
		{
			if (!PathEx.Exists(path))
				throw new ArgumentException();

			string filename = System.IO.Path.GetFileName(path);
			var nameMatch = NameRegex.Match(filename);

			if (!nameMatch.Success)
				throw new NotAModException($"Given file/folder \"{filename}\" did not match the name pattern. Path: \"{path}\"");

			var name = nameMatch.Groups["name"].Value;

			var infoSerializerSettings = new JsonSerializerSettings
			{
				Formatting = Formatting.Indented,
				ContractResolver = new DefaultContractResolver
				{
					NamingStrategy = new SnakeCaseNamingStrategy()
				}
			};
			
			string infoFileContent = "";
			if (System.IO.File.Exists(path))
			{
				using (var stream = new FileStream(path, FileMode.Open))
				using (var archive = new ZipArchive(stream))
				{
					var entry = archive.Entries.FirstOrDefault(e => InfoJsonRegex.IsMatch(e.FullName));

					if (entry != null)
					{
						using (var entrySteam = entry.Open())
						using (var reader = new StreamReader(entrySteam))
						{
							infoFileContent = reader.ReadToEnd();
						}
					}

				}
			}
			else
				infoFileContent = System.IO.File.ReadAllText(System.IO.Path.Combine(path, "info.json"));
			
			var infoFile = JsonConvert.DeserializeObject<ModInfoFile>(infoFileContent, infoSerializerSettings) ?? new ModInfoFile();

			return new Mod
			{
				Name = name,
				Author = infoFile.Author,
				Title = infoFile.Title ?? name,
				Description = infoFile.Description,
				Version = Version.Parse(nameMatch.Groups["version"].Value),
				Path = path,
				Enabled = enabledList.GetOrDefault(name, defaultOn)
			};
		}


		public ModId GetId()
		{
			return new ModId(this.Name, this.Version) { IsArchive = this.IsArchive };
		}

		public static implicit operator ModId(Mod mod)
		{
			return mod.GetId();
		}


		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
		{
			PropertyChanged?.Invoke(this, e);
		}

		public int CompareTo(Mod other)
		{
			if (ReferenceEquals(this, other)) return 0;
			if (ReferenceEquals(null, other)) return 1;
			var nameComparison = string.Compare(Name, other.Name, StringComparison.OrdinalIgnoreCase);
			if (nameComparison != 0) return nameComparison;
			return Comparer<Version>.Default.Compare(Version, other.Version);
		}
	}
}
