﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace CFAN.Model
{
	public class ModId : IEquatable<ModId>, IComparable<ModId>
	{
		public ModId(string name, Version version)
		{
			Name = name;
			Version = version;
		}

		public string Name { get; }


		public Version Version { get; }

		public bool IsArchive { get; set; }

		[JsonIgnore]
		public string PathName => $"{this.Name}_{this.Version}{(this.IsArchive ? ".zip" : "")}";


		public bool Equals(ModId other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return string.Equals(Name, other.Name) && Equals(Version, other.Version);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((ModId) obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				return ((Name != null ? Name.GetHashCode() : 0) * 397) ^ (Version != null ? Version.GetHashCode() : 0);
			}
		}

		public int CompareTo(ModId other)
		{
			if (ReferenceEquals(this, other)) return 0;
			if (ReferenceEquals(null, other)) return 1;
			var nameComparison = string.Compare(Name, other.Name, StringComparison.Ordinal);
			if (nameComparison != 0) return nameComparison;
			return Comparer<Version>.Default.Compare(Version, other.Version);
		}
	}
}
