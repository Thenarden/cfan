﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CFAN.Factorio.Api;
using Newtonsoft.Json;

namespace CFAN.Model
{
    public class ProfileManifest
    {
	    public int ManifestVersion { get; set; } = 1;

        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Author { get; set; }

        public string Description { get; set; }

        public Version ProfileVersion { get; set; }

        public Version GameVersion { get; set; }

        [JsonIgnore]
        public Version MainGameVersion => new Version(this.GameVersion.Major, this.GameVersion.Minor);


        public string IconFile { get; set; }


		public List<ModId> DownloadedMods { get; set; }

		public List<ModId> IncludedMods { get; set; }

		
    }

}
