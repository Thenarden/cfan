﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using CFAN.Model;
using CFAN.UI;
using CFAN.UI.ViewModel;
using CFAN.Utils;

namespace CFAN
{
	class Program
	{
		[STAThread]
		static void Main(string[] args)
		{
			new Program();
		}

		private readonly App application;
		private readonly MainWindow mainWindow;


		Program()
		{

			var context = new AppContext();
			
			application = new App(context)
			{
				ShutdownMode = ShutdownMode.OnExplicitShutdown
			};
			application.InitializeComponent();

			mainWindow = new MainWindow();
			mainWindow.OnExit += application.Shutdown;

			var profiles = ProfileListViewModel.FromObservable(context.Profile);

			mainWindow.View = profiles;

			if (!context.Credentials.ContainsDefaultCredentials)
				mainWindow.Popup = new DefaultCredentialsViewModel();

			mainWindow.Visibility = Visibility.Visible;

			var t = new Thread(() =>
			{
				Thread.Sleep(10000);
				context.Profile.Add(new Profile("Test 3", Path.Combine(context.Settings.DefaultProfileFolder, "Test 3")));
			});
			//t.Start();

			application.Run();
		}
	}
}
