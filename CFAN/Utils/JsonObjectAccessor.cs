﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CFAN.Utils
{
	public class JsonObjectAccessor
	{
		public JObject Object { get; }
		public JsonSerializer Serializer { get; }

		public JsonObjectAccessor(JObject @object, JsonSerializer serializer)
		{
			this.Object = @object;
			this.Serializer = serializer;
		}

		public TR Deserialize<TR>(string name)
		{
			return this.Serializer.Deserialize<TR>(new JTokenReader(this.Object[name]));
		}
		public TR Deserialize<TR>(object key)
		{
			return this.Serializer.Deserialize<TR>(new JTokenReader(this.Object[key]));
		}

		public object Deserialize(string name)
		{
			return this.Serializer.Deserialize(new JTokenReader(this.Object[name]));
		}
		public object Deserialize(object key)
		{
			return this.Serializer.Deserialize(new JTokenReader(this.Object[key]));
		}
	}
}
