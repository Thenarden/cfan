﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace CFAN.Utils
{
	public class SimpleCommand : ICommand
	{
		public Predicate<object> CanExecuteDelegate { get; set; }
		public Action<object> ExecuteDelegate { get; set; }

		public SimpleCommand()
		{

		}
	    public SimpleCommand(Action executeDelegate)
        : this(o => executeDelegate())
	    {}
		public SimpleCommand(Action<object> executeDelegate)
		{
			this.ExecuteDelegate = executeDelegate;
		}

		#region ICommand Members

		public bool CanExecute(object parameter)
		{
			if (CanExecuteDelegate != null)
				return CanExecuteDelegate(parameter);
			return true;// if there is no can execute default to true
		}

		public event EventHandler CanExecuteChanged
		{
			add => CommandManager.RequerySuggested += value;
			remove => CommandManager.RequerySuggested -= value;
		}

		public void Execute(object parameter)
		{
			ExecuteDelegate?.Invoke(parameter);
		}

		#endregion
	}
	public class SimpleCommand<T> : ICommand
	{
		public Predicate<T> CanExecuteDelegate { get; set; }
		public Action<T> ExecuteDelegate { get; set; }

		public SimpleCommand()
		{

		}
		public SimpleCommand(Action<T> executeDelegate)
		{
			this.ExecuteDelegate = executeDelegate;
		}

		#region ICommand Members

		public bool CanExecute(object parameter)
		{
			if (CanExecuteDelegate != null)
				return CanExecuteDelegate((T)parameter);
			return true;// if there is no can execute default to true
		}

		public event EventHandler CanExecuteChanged
		{
			add => CommandManager.RequerySuggested += value;
			remove => CommandManager.RequerySuggested -= value;
		}

		public void Execute(object parameter)
		{
			ExecuteDelegate?.Invoke((T)parameter);
		}

		#endregion
	}
}
