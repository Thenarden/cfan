﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup.Localizer;
using Shortbit.Utils;
using Shortbit.Utils.Collections.Generic;

namespace CFAN.Utils
{
    public class ObservableDictionaryValues <TKey, TValue> : ICollection<TValue>, INotifyPropertyChanged, INotifyCollectionChanged
    {
		public ObservableDictionary<TKey, TValue> Dictionary { get; }

		public ObservableDictionaryValues(ObservableDictionary<TKey, TValue> dictionary)
		{
			this.Dictionary = dictionary;
			this.Dictionary.CollectionChanged += DictionaryOnCollectionChanged;
		}

	    private void DictionaryOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs args)
	    {
		    NotifyCollectionChangedEventArgs nArgs;

		    switch (args.Action)
		    {
				case NotifyCollectionChangedAction.Reset:
					nArgs = new NotifyCollectionChangedEventArgs(args.Action);
					break;
				case NotifyCollectionChangedAction.Add:
				case NotifyCollectionChangedAction.Remove:
					nArgs = new NotifyCollectionChangedEventArgs(
						args.Action, 
						args.NewItems.Cast<KeyValuePair<TKey, TValue>>().Select(p => p.Value).ToListSafe(), 
						args.NewStartingIndex);
					break;
				case NotifyCollectionChangedAction.Move:
					nArgs = new NotifyCollectionChangedEventArgs(
						args.Action,
						args.NewItems.Cast<KeyValuePair<TKey, TValue>>().Select(p => p.Value).ToListSafe(), 
						args.NewStartingIndex,
						args.OldStartingIndex);
					break;
				case NotifyCollectionChangedAction.Replace:
					nArgs = new NotifyCollectionChangedEventArgs(
						args.Action,
						args.NewItems.Cast<KeyValuePair<TKey, TValue>>().Select(p => p.Value).ToListSafe(),
						args.OldItems.Cast<KeyValuePair<TKey, TValue>>().Select(p => p.Value).ToListSafe());
					break;

				default:
					throw new NotSupportedException();
			}
			this.OnCollectionChanged(nArgs);
	    }

	    public IEnumerator<TValue> GetEnumerator() => Dictionary.Values.GetEnumerator();

	    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

	    public event PropertyChangedEventHandler PropertyChanged
	    {
		    add => this.Dictionary.PropertyChanged += value;
		    remove => this.Dictionary.PropertyChanged -= value;
		}
	    public event NotifyCollectionChangedEventHandler CollectionChanged;

	    protected virtual void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
	    {
		    CollectionChanged?.Invoke(this, e);
	    }



	    public void Add(TValue item)
		{
			this.Dictionary.Values.Add(item);
		}

	    public void Clear()
		{
			this.Dictionary.Values.Clear();
		}

	    public bool Contains(TValue item)
	    {
		    return this.Dictionary.Values.Contains(item);
	    }

	    public void CopyTo(TValue[] array, int arrayIndex)
	    {
		    this.Dictionary.Values.CopyTo(array, arrayIndex);
	    }

	    public bool Remove(TValue item)
		{
			return this.Dictionary.Values.Remove(item);
		}

	    public int Count => this.Dictionary.Values.Count;
	    public bool IsReadOnly => this.Dictionary.Values.IsReadOnly;
    }
	
}
