﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CFAN.Utils.WP7Contrib
{
	public class ValueChangedEventArgs : EventArgs
	{
		private readonly object newValue;

		private readonly object oldValue;

		public ValueChangedEventArgs(object oldValue, object newValue)
		{
			this.oldValue = oldValue;
			this.newValue = newValue;
		}

		public object NewValue
		{
			get
			{
				return this.newValue;
			}
		}

		public object OldValue
		{
			get
			{
				return this.oldValue;
			}
		}
	}
}
