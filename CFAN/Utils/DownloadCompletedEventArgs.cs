﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CFAN.Utils
{
	public class DownloadCompletedEventArgs : EventArgs
	{
		public DownloadCompletedEventArgs(long downloaded)
		{
			Downloaded = downloaded;
		}

		public long Downloaded { get; }
	}
}
