﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace CFAN.Utils
{
    public class FocusElementCommand : ICommand
    {
        public bool CanExecute(object parameter)
        {
            return parameter is UIElement;
        }

        public void Execute(object parameter)
        {
            var element = parameter as UIElement;
            element?.Focus();
        }
        
        public event EventHandler CanExecuteChanged
        {
            add => CommandManager.RequerySuggested += value;
            remove => CommandManager.RequerySuggested -= value;
        }
    }
}
