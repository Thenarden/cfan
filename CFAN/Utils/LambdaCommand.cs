﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace CFAN.Utils
{
    public class LambdaCommand <T> : ICommand
    {
		public Action<T> Action { get; }
	    public LambdaCommand(Action<T> action)
	    {
		    this.Action = action;
	    }

		public bool CanExecute(object parameter)
		{
			return parameter is T;
		}

	    public void Execute(object parameter)
	    {
		    this.Action((T) parameter);
	    }

		public static implicit  operator LambdaCommand<T>(Action<T> action)
			=> new LambdaCommand<T>(action);

	    public event EventHandler CanExecuteChanged;

	    protected virtual void OnCanExecuteChanged()
	    {
		    CanExecuteChanged?.Invoke(this, EventArgs.Empty);
	    }
    }
}
