﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shortbit.Utils;

namespace CFAN.Utils
{
    public static class FileUtils
    {
        public static bool IsArchiveFile(string path)
        {
            Throw.IfNullOrWhitespace(path, nameof(path));

            if (Directory.Exists(path))
                return false;
            if (File.Exists(path))
                return true;

            return path.EndsWith(".zip");
        }
    }
}
