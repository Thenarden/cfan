﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CFAN.Utils
{
	public class BlockDownloadCompletedEventArgs : EventArgs
	{
		public BlockDownloadCompletedEventArgs(int blockSize, long downloaded, long remaining, long size)
		{
			Downloaded = downloaded;
			Remaining = remaining;
			Size = size;
			BlockSize = blockSize;
		}

		public int BlockSize { get; }
		
		public long Downloaded { get; }

		public long Remaining { get; }

		public long Size { get; }
	}
}
