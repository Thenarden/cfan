﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace CFAN.Utils
{
	class ListBoxWidthFixConverter : IValueConverter
	{
		public double Offset { get; set; } = -30;

		public object Convert(
			object value,
			Type targetType,
			object parameter,
			System.Globalization.CultureInfo culture
		)
		{
			return (double)value + this.Offset;
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
