﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CFAN.Utils
{
	public class FactoryConverter<T> : JsonConverter where T : class 
	{
		

		private readonly Func<JObject, JsonSerializer, T> factory;


		public FactoryConverter(Func<T> factory)
		{
			this.factory = (o, serializer) => factory();
		}
		public FactoryConverter(Func<JsonObjectAccessor, T> factory)
		{
			this.factory = (o, serializer) => factory(new JsonObjectAccessor(o, serializer));
		}
		public FactoryConverter(Func<JObject, JsonSerializer, T> factory)
		{
			this.factory = factory;
		}


		public override bool CanConvert(Type objectType)
		{
			return objectType == typeof(T);
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			var obj = JObject.Load(reader);
			var inst = this.factory(obj, serializer);

			serializer.Populate(new JTokenReader(obj), inst);
			return inst;
		}


		public override bool CanWrite => false;

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			throw new NotSupportedException("FactoryConverter should only be used to deserialize.");
		}
	}
}
