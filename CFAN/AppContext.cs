﻿using CFAN.Context;
using CFAN.Factorio.Adapter;

namespace CFAN
{
	public class AppContext
	{
		public AppContext(string settingsFile = null)
			: this(AppSettings.LoadFromFileOrDefault(settingsFile))
		{ }
		
		public AppContext(AppSettings settings)
		{
			this.Settings = settings;
            this.Tasks = new TaskManager(this.Settings);
			this.Credentials = new CredentialsManager(settings);
            this.Cache = new CacheManager(this.Settings);

            this.Downloads = new DownloadManager(this);
			this.Profile = new ProfileManager(this);
            this.Mods = new ModManager(this);

			this.GameVersionAdapter = new WebsiteAdapter(this);
			this.ModInfoAdapter = new WebsiteAdapter(this);
		}

		public AppSettings Settings { get; }

        public TaskManager Tasks { get; }

		public CredentialsManager Credentials { get; }

        public CacheManager Cache { get; }

		public DownloadManager Downloads { get; }

		public ProfileManager Profile { get; }

        public ModManager Mods { get; }


		public IGameVersionAdapter GameVersionAdapter { get; }

		public IModInfoAdapter ModInfoAdapter { get; }
		
	}
}
